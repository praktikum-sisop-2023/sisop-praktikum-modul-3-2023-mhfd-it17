# Sisop-Praktikum-Modul-3-2023-MHFD-IT17

Anggota Kelompok : 
1. Salmaa Satifha Dewi Rifma Putri  (5027211011)
2. Yoga Hartono                     (5027211023)
3. Dzakirozaan Uzlahwasata          (5027211066)


# Soal 1
## Analisis Soal
Dari soal diminta untuk mengkompres file dari link https://drive.google.com/file/d/1HWx1BGi1rEZjoEjzhAuKMAcdI27wQ41C/view?usp=share_link

Ada ketentuan yang harus dipenuhi yaitu :
- Wajib menerapkan konsep 2 pipe dan fork.  
- Karakter selain huruf tidak dimasukkan ke perhitungan jumlah bit dan tidak dihitung frekuensi kemunculannya.
- Mengubah seluruh huruf kecil ke kapital.
- Membaca file asli dan menghitung frekuensi munculnya huruf di parent process, kemudian hasilnya dikirimkan ke child process.
- Di child process melakukan kompresi algoritma huffman berdasarkan jumlah frekuensi huruf.
- Masih di child process, huffman tree disimpan ke file yang sudah terkompresi. Karakter diubah menjadi kode huffman.
- Kode huffman dikirim ke program dekompresi dengan pipe.
- Hasil kompresi dikirim ke parent process, huffman tree dari file terkompresi dibaca dan didekompresi.
- Hitung jumlah bit setelah dikompresi dan perbandingannya dengan sebelum kompresi parent process.
- Tampilkan jumlah bit dan perbandingan ke layar.

## Cara Pengerjaan
- MEMBUAT FOLDER modul3no1
```
mkdir modul3no1
```
- MEMBUAT FILE DENGAN NAMA lossless.c
```
nano lossless.c
```
- MENDEKLARASIKAN STRUCT NODE
```
struct huffmanNode {
    int frekuensi;
    char huruf;
    struct huffmanNode *parent;
    struct huffmanNode *anak_kanan;
    struct huffmanNode *anak_kiri;
};
```
    - frekuensi : integer yang menyimpan frekuensi dari suatu karakter
    - huruf : karakter yang frekuensinya akan dicatat dalam atribut frekuensi
    - parent : pointer ke huffmanNode parent dari node saat ini dalam sebuah pohon huffman
    - anak_kanan : pointer ke huffmanNode anak kanan dari node saat ini dalam sebuah pohon huffman
    - anak_kiri : pointer ke huffmanNode anak kiri dari node saat ini dalam sebuah pohon huffman

- MEMBUAT NODE BARU
```
struct huffmanNode *nodeBaruHuff(char huruf, int frekuensi){
    struct huffmanNode *node = (struct huffmanNode *)malloc(sizeof(struct huffmanNode));
    node->huruf = huruf;
    node->frekuensi = frekuensi;
    node->anak_kiri = NULL;
    node->anak_kanan = NULL;
    return node;
}
```
- struct huffmanNode *nodeBaruHuff(char huruf, int frekuensi){ - Mendefinisikan sebuah fungsi bernama nodeBaruHuff yang menerima dua parameter: karakter huruf dan integer frekuensi. Fungsi ini mengembalikan pointer ke struct huffmanNode.
- struct huffmanNode *node = (struct huffmanNode *)malloc(sizeof(struct huffmanNode)); - Membuat variabel pointer node yang menunjuk ke alamat memori yang baru dialokasikan untuk struct huffmanNode menggunakan fungsi malloc(). Ukuran memori yang dialokasikan adalah ukuran dari struct huffmanNode.
- node->huruf = huruf; - Memberikan nilai dari parameter huruf ke variabel huruf dalam struct huffmanNode.
- node->frekuensi = frekuensi; - Memberikan nilai dari parameter frekuensi ke variabel frekuensi dalam struct huffmanNode.
- node->anak_kiri = NULL; - Menginisialisasi pointer anak_kiri dalam struct huffmanNode dengan NULL.
- node->anak_kanan = NULL; - Menginisialisasi pointer anak_kanan dalam struct huffmanNode dengan NULL.
- return node; - Mengembalikan pointer ke struct huffmanNode yang baru dibuat dengan nilai-nilai yang telah ditetapkan sebelumnya.

- MEMBANDINGKAN FREKUENSI DARI 2 NODE HUFFMAN
```
int banding_nodeArray(const void *p, const void *x){
    return (*(struct huffmanNode **)p)->frekuensi - (*(struct huffmanNode **)x)->frekuensi;
}
```
- Fungsi banding_nodeArray : fungsi yang digunakan dalam pengurutan array of pointer pada proses pembuatan pohon Huffman. Fungsi ini memiliki 2 parameter, yaitu p dan x, yang bertipe void *.

- Karena kedua pointer void ini sebenarnya merujuk pada alamat node huffman, maka perlu dilakukan sorting untuk mengubah pointer void menjadi pointer ke node huffman. Kedua pointer void diubah menjadi pointer ke pointer node huffman (struct huffmanNode **).

- Kemudian, fungsi akan mengembalikan selisih antara frekuensi dari dua node huffman yang dibandingkan. Jika hasil pengurangan positif, artinya frekuensi node pertama lebih besar dari node kedua. Sebaliknya, jika hasil pengurangan negatif, artinya frekuensi node pertama lebih kecil dari node kedua. Jika hasil pengurangan sama dengan nol, artinya frekuensi node pertama sama dengan node kedua.

- MEMBUAT BINARY TREE HUFFMAN
```
// Membuat binary tree huffman
struct huffmanNode *buat_huffman_tree(char *data, int *frekuensi){
    //inisialisasi array dari node huffman untuk tiap simbol pada data
    struct huffmanNode *nodeArray[CHAR_MAX] = {NULL};
    int n = 0;
    int i = 0;
    while (i < strlen(data)){
        char huruf = toupper(data[i]);
        if (isalpha(huruf)) {
            // mengecek jika huruf sudah pernah di temukan
            int j = 0;
            while (j < n)
            {
                if (nodeArray[j]->huruf == huruf)
                {
                    nodeArray[j]->frekuensi++;
                    break;
                }
                j++;
            }
            // jika belum, dibuat node baru
            if (j == n)
            {
                nodeArray[j] = nodeBaruHuff(huruf, frekuensi[huruf]);
                n++;
            }
        }
        i++;
    }
    // sorting node huffman berdasarkan frekuensi
    qsort(nodeArray, n, sizeof(struct huffmanNode *), banding_nodeArray);

    // membuat binary tree huffman 
    while (n > 1) {
        struct huffmanNode *nodeP = nodeArray[0];
        struct huffmanNode *nodeX = nodeArray[1];
        struct huffmanNode *parent = nodeBaruHuff('$', nodeP->frekuensi + nodeX->frekuensi);
        parent->anak_kiri = nodeP;
        parent->anak_kanan = nodeX;
        // menghapus nodeP dan nodeX
        for (int i = 2; i < n; i++) {
            memcpy(&nodeArray[i - 2], &nodeArray[i], sizeof(*nodeArray));
        }
        nodeArray[n - 2] = parent;
        n--;
        // sorting lagi
        qsort(nodeArray, n, sizeof(*nodeArray), banding_nodeArray);
    }
    // nodearray merupakan root dari huffman binary tree
    return nodeArray[0];
}
```
- Fungsi buat_huffman_tree menerima dua parameter, yaitu data yang merupakan string yang akan dijadikan basis untuk membuat binary tree huffman, dan frekuensi yang merupakan array yang berisi frekuensi kemunculan setiap karakter pada data.
- Inisialisasi array nodeArray dari pointer ke node huffman dan menyimpan node huffman setiap karakter yang ada pada data.
- Dilakukan loop pada data untuk memproses setiap karakter yang terdapat pada data. Pada setiap iterasi, program akan mengecek apakah karakter tersebut merupakan huruf atau tidak.
- Jika karakter adalah huruf, akan dicek apakah sudah pernah ditemukan atau belum pada nodeArray. Jika sudah, frekuensi dari node huffman yang sudah ada akan di-increment. Jika belum, program akan membuat node baru dengan fungsi nodeBaruHuff dan menambahkannya ke dalam nodeArray.
- Setelah selesai memproses semua karakter pada data, nodeArray akan di-sorting berdasarkan frekuensi dengan menggunakan fungsi qsort.
- Dilakukan loop untuk menggabungkan node huffman pada nodeArray sehingga membentuk binary tree huffman. Pada setiap iterasi, program akan mengambil dua node huffman dengan frekuensi terkecil pada nodeArray dan menggabungkannya dengan membuat node parent baru dengan frekuensi yang sama dengan jumlah frekuensi kedua node tersebut. Setelah itu, program akan menghapus dua node huffman tersebut dari nodeArray dan menambahkan node parent baru ke dalam nodeArray. Langkah ini akan diulang sampai hanya tersisa satu node huffman pada nodeArray.
- Setelah loop selesai, nodeArray hanya akan memiliki satu elemen, yaitu root dari binary tree huffman. Node ini akan dikembalikan sebagai hasil dari fungsi buat_huffman_tree.

- MENGHITUNG JUMLAH BIT YANG DIBUTUHKAN DALAM KOMPRESI
```
int total_dan_kompresi(struct huffmanNode *root, int length){
    if (!root){
        return 0;
    }
    else if(root->anak_kiri == NULL && root->anak_kanan == NULL){
        return root->frekuensi * length;
    }
    else{
        return total_dan_kompresi(root->anak_kiri, length + 1) + total_dan_kompresi(root->anak_kanan, length + 1);
    }
}
```
- int total_dan_kompresi(struct huffmanNode *root, int length){ : fungsi ini mengambil dua parameter yaitu pointer ke node root dari Huffman binary tree dan panjang bit saat ini. Fungsi ini mengembalikan total bit yang dibutuhkan untuk mengompres data.
- if (!root){ return 0; } : Jika node root kosong, artinya tidak ada data yang perlu dikompres, sehingga mengembalikan nilai 0.
- else if(root->anak_kiri == NULL && root->anak_kanan == NULL){ return root->frekuensi * length; } : Jika node yang diberikan adalah leaf node, maka jumlah bit yang dibutuhkan untuk merepresentasikan node ini sama dengan frekuensi node dikalikan dengan panjang bit saat ini.
- else{ return total_dan_kompresi(root->anak_kiri, length + 1) + total_dan_kompresi(root->anak_kanan, length + 1); } : Jika node bukan leaf node, maka fungsi akan memanggil dirinya sendiri untuk menghitung total bit yang dibutuhkan untuk anak kiri dan anak kanan node saat ini. Panjang bit saat ini juga ditingkatkan sebesar satu karena level node telah naik satu tingkat. Fungsi akan mengembalikan jumlah bit yang dibutuhkan untuk mengompresi data.

- MEMBACA HUFFMAN TREE DARI FILE TERKOMPRESI
```
// C. membaca Huffman tree dari file terkompresi
struct huffmanNode *baca_huffman_tree(char *namaFile){
    FILE *file = fopen(namaFile, "r");
    if (file == NULL){
        printf("Gagal membuka file\n");
        exit(1);
    }
    //membaca karakter pertama dr file
    char ch = fgetc(file);
    if (ch == EOF){
        printf("File kosong\n");
        exit(1);
    }
    //root dr huffman tree yg dibaca file
    struct huffmanNode *root = nodeBaruHuff('$', 0);
    struct huffmanNode *cur_node = root;

    while (ch != EOF){ 
        //karakter = 0, dicek ada anak kiri, akan dibuat node baru
        if (ch == '0'){
            if (cur_node->anak_kiri == NULL){
                cur_node->anak_kiri = nodeBaruHuff('$', 0);
            }
            cur_node = cur_node->anak_kiri;
        }
        //karakter = 1, cek anak kanan
        else if (ch == '1'){
            if (cur_node->anak_kanan == NULL){
                cur_node->anak_kanan = nodeBaruHuff('$', 0);
            }
            cur_node = cur_node->anak_kanan;
        }
        ch = fgetc(file);
    }
    fclose(file);
    return root;
}
```
- Proses membaca dimulai dengan membuka file dengan fungsi fopen dan memeriksa apakah file dapat dibuka dengan sukses. 
- Pembacaan karakter pertama dari file dan dibuat sebuah Huffman node baru untuk root Huffman tree yang akan dibaca dari file. 
- Pembacaan karakter selanjutnya dari file dan dilakukan pengecekan apakah karakter tersebut adalah '0' atau '1'. 
- Jika karakter adalah '0', dibuat Huffman node baru untuk anak kiri dari Huffman node saat ini. Jika karakter adalah '1', dibuat sebuah Huffman node baru untuk anak kanan dari Huffman node saat ini. 
- Setelah semua karakter dari file dibaca, file akan ditutup dan root dari Huffman tree yang dibaca akan dikembalikan sebagai output dari fungsi.

- MENCARI KODE HUFFMAN TIAP HURUF DI BINARY TREE DAN DILAKUKAN KOMPRESI
```
void cari_huffmanKode(struct huffmanNode *root, char *kode, int length_kode, char *inpFile, char *kompres){
    if (root->anak_kiri == NULL && root->anak_kanan == NULL) {
        kode[length_kode] = '\0';
        printf("%c: %s\n", root->huruf, kode);
        int i = 0;
        while (i < strlen(inpFile)){
            if (toupper(inpFile[i]) == root->huruf){
                strcat(kompres, kode);
            }
            i++;
        }
        return;
    } 
    kode[length_kode] = '0';
    cari_huffmanKode(root->anak_kiri, kode, length_kode + 1, inpFile, kompres);
    kode[length_kode] = '1';
    cari_huffmanKode(root->anak_kanan, kode, length_kode + 1, inpFile, kompres);
}
```
- struct huffmanNode *root: node root dari binary tree Huffman yang akan dicari kode Huffmannya.
- char *kode: array untuk menyimpan kode Huffman yang ditemukan.
- int length_kode: panjang kode Huffman yang sudah ditemukan.
- char *inpFile: string yang akan dikompresi menggunakan kode Huffman yang ditemukan.
- Fungsi rekursi menelusuri seluruh node pada binary tree Huffman. Ketika ditemukan node leaf (yaitu node yang tidak memiliki anak kiri dan anak kanan), fungsi akan mencetak kode Huffman untuk huruf yang sesuai dan menggabungkan kode Huffman tersebut dengan string kompres untuk setiap kemunculan huruf dalam inpFile.
- Selanjutnya, fungsi memanggil diri sendiri untuk mencari kode Huffman untuk anak kiri dan anak kanan dari node saat ini. Untuk mencari kode Huffman anak kiri, bit 0 ditambahkan pada kode dan kemudian fungsi dipanggil kembali dengan node anak kiri. Begitu pula untuk mencari kode Huffman anak kanan, bit 1 ditambahkan pada kode dan fungsi dipanggil kembali dengan node anak kanan. Seluruh binary tree Huffman akan ditelusuri dan kode Huffman untuk setiap huruf akan ditemukan.

- MEMBUAT FUNGSI UTAMA UNTUK MEMANGGIL FUNGSI YANG SUDAH DIBUAT
```
int main(int argc, char *argv[]){
    //mengecek jumlah argument 
    if (argc != 2){
        fprintf(stderr, "Usage: %s [namaFile]\n", argv[0]);
        exit(1);
    }

    char *namaFile = argv[1];
    int fd = open(namaFile, O_RDWR);
    if (fd == -1){
        perror("tidak bisa mengakses file");
        return 1;
    }

    // inisialisasi 2 pipe
    int fd1[2]; // menyimpan dua ujung pipa pertama
    int fd2[2]; // menyimpan dua ujung pipa kedua

    if (pipe(fd1) < 0 || pipe(fd2) < 0) {
        perror("Pipe Failed");
        return 1;
    }

    pid_t p;
    p = fork();

    if (p < 0){
        fprintf(stderr, "fork Failed");
        return 1;
    }

    // Parent process
    else if (p > 0){
        // Menutup pembacaan akhir pipa pertama
        close(fd1[0]); 
        // Menutup pembacaan akhir pipa kedua
        close(fd2[1]); 

        int frekuensi[CHAR_MAX] = {0};
        char huruf;
        int total_bits = 0;
        while (read(fd, &huruf, 1) > 0){
            if (isalpha(huruf)){
                huruf = toupper(huruf);
                frekuensi[(int)huruf]++;
                // Menambah 8 bit di setiap huruf yang ditemukan
                total_bits += 8;
            }
        }

        int i = 0;
        while (i < CHAR_MAX){
            if (frekuensi[i] > 0){
                if (isalpha(i)) {
                    printf("%c: %d\n", i, frekuensi[i]);
                } else {
                    printf("'%c': %d\n", i, frekuensi[i]);
                }
            }
            i++;
        }

        printf("\nSebelum kompress(bit): %d\n", total_bits);
        close(fd);

        // mengirim frekuensi ke child process
        write(fd1[1], frekuensi, sizeof(frekuensi));
        close(fd1[1]);

        // menunggu untuk mengirim bit yang dikompress
        int bit_kompres;
        read(fd2[0], &bit_kompres, sizeof(bit_kompres));
        close(fd2[0]);

        float perbandinganKompres = (float)bit_kompres / total_bits;

        return 0;
    }
    // Child process
    else{
        close(fd1[1]); // menutup tulisan ujung pipa pertama
        close(fd2[0]); // menutup tulisan ujung pipa kedua

        // membaca frekuensi dari parent process
        int frekuensi[CHAR_MAX] = {0};
        read(fd1[0], frekuensi, sizeof(frekuensi));
        close(fd1[0]);

        //mengoversi isi file ke bentuk frekuensi masing-masing karakter
        char inpFile[CHAR_MAX * 100] = "";
        int i = 0;
        while (i < CHAR_MAX){
            int j = 0;
            while (j < frekuensi[i]){
                char huruf = (char)i;
                strncat(inpFile, &huruf, 1);
                j++;
            }
            i++;
        }

        struct huffmanNode *root = buat_huffman_tree(inpFile, frekuensi);
        char kode[CHAR_MAX] = "";
        char kompres[CHAR_MAX * 100] = "";
        cari_huffmanKode(root, kode, 0, inpFile, kompres);

        int bit_kompres = strlen(kompres);
        printf("Setelah kompress(bit): %d\n", bit_kompres);
        float perbandinganKompres = (float)bit_kompres / total_dan_kompresi(root, 0);
        printf("Perbandingan: %.2f\n", perbandinganKompres);

        // mengirim hasil kompresi ke parent
        write(fd2[1], &bit_kompres, sizeof(bit_kompres));
        close(fd2[1]);
        exit(0);
    }
}
```
- Fungsi main terdiri dari beberapa bagian. Pertama-tama, program mengecek jumlah argumen yang diberikan. Jika jumlah argumen bukan 2, program akan mencetak pesan kesalahan dan keluar (exit) dari program. Jika jumlah argumen tepat, program akan membuka file dengan nama yang diberikan sebagai argumen menggunakan fungsi open. Jika tidak bisa membuka file, program akan mencetak pesan kesalahan dan keluar dari program.

- Selanjutnya, program melakukan inisialisasi dua pipa (pipe) menggunakan fungsi pipe. Jika inisialisasi gagal, program mencetak pesan kesalahan dan keluar dari program. Kemudian, program melakukan fork untuk membuat proses child. Jika fork gagal, program mencetak pesan kesalahan dan keluar dari program.

- Jika proses yang berjalan adalah proses parent, program akan menutup ujung pembacaan pipa pertama dan menutup ujung penulisan pipa kedua. Program kemudian membaca isi file dan menghitung frekuensi masing-masing karakter yang ditemukan dalam file. Frekuensi ini kemudian dikirimkan ke proses child melalui pipa pertama. Program kemudian menunggu untuk menerima bit yang telah dikompresi dari proses child melalui pipa kedua. Setelah menerima bit yang dikompresi, program mencetak jumlah bit sebelum dan sesudah kompresi, serta perbandingan antara kedua jumlah tersebut. Program kemudian keluar dari fungsi main.

- Jika proses yang berjalan adalah proses child, program akan menutup ujung penulisan pipa pertama dan menutup ujung pembacaan pipa kedua. Program kemudian membaca frekuensi masing-masing karakter yang dikirimkan oleh proses parent melalui pipa pertama. Frekuensi ini digunakan untuk membuat pohon Huffman yang digunakan untuk mengompresi isi file. Setelah melakukan kompresi, program mengirimkan hasil kompresi dalam bentuk bit ke proses parent melalui pipa kedua. Program kemudian keluar dari fungsi main.













## Source Code
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <ctype.h>

#define CHAR_MAX 1024

// deklarasi struct node binary tree huffman
struct huffmanNode {
    int frekuensi;
    char huruf;
    struct huffmanNode *parent; // pointers (parent, anak_kiri and anak_kanan)
    struct huffmanNode *anak_kanan; // pointers (parent, anak_kiri and anak_kanan)
    struct huffmanNode *anak_kiri; // pointers (parent, anak_kiri and anak_kanan)
};

// Membuat node baru untuk huffman binary tree
struct huffmanNode *nodeBaruHuff(char huruf, int frekuensi){
    struct huffmanNode *node = (struct huffmanNode *)malloc(sizeof(struct huffmanNode));
    node->huruf = huruf;
    node->frekuensi = frekuensi;
    node->anak_kiri = NULL;
    node->anak_kanan = NULL;
    return node;
}

// Membandingkan frekuensi dari 2 node huffman
int banding_nodeArray(const void *p, const void *x){
    return (*(struct huffmanNode **)p)->frekuensi - (*(struct huffmanNode **)x)->frekuensi;
}

// Membuat binary tree huffman
struct huffmanNode *buat_huffman_tree(char *data, int *frekuensi){
    //inisialisasi array dari node huffman untuk tiap simbol pada data
    struct huffmanNode *nodeArray[CHAR_MAX] = {NULL};
    int n = 0;
    int i = 0;
    while (i < strlen(data)){
        char huruf = toupper(data[i]);
        if (isalpha(huruf)) {
            // mengecek jika huruf sudah pernah di temukan
            int j = 0;
            while (j < n)
            {
                if (nodeArray[j]->huruf == huruf)
                {
                    nodeArray[j]->frekuensi++;
                    break;
                }
                j++;
            }
            // jika belum, dibuat node baru
            if (j == n)
            {
                nodeArray[j] = nodeBaruHuff(huruf, frekuensi[huruf]);
                n++;
            }
        }
        i++;
    }
    // sorting node huffman berdasarkan frekuensi
    qsort(nodeArray, n, sizeof(struct huffmanNode *), banding_nodeArray);

    // membuat binary tree huffman 
    while (n > 1) {
        struct huffmanNode *nodeP = nodeArray[0];
        struct huffmanNode *nodeX = nodeArray[1];
        struct huffmanNode *parent = nodeBaruHuff('$', nodeP->frekuensi + nodeX->frekuensi);
        parent->anak_kiri = nodeP;
        parent->anak_kanan = nodeX;
        // menghapus nodeP dan nodeX
        for (int i = 2; i < n; i++) {
            memcpy(&nodeArray[i - 2], &nodeArray[i], sizeof(*nodeArray));
        }
        nodeArray[n - 2] = parent;
        n--;
        // sorting lagi
        qsort(nodeArray, n, sizeof(*nodeArray), banding_nodeArray);
    }
    // nodearray merupakan root dari huffman binary tree
    return nodeArray[0];

}

// menghitung jumlah bit yang dibutuhkan untuk mengompres data menggunakan Huffman binary tree
int total_dan_kompresi(struct huffmanNode *root, int length){
    if (!root){
        return 0;
    }
    else if(root->anak_kiri == NULL && root->anak_kanan == NULL){
        return root->frekuensi * length;
    }
    else{
        return total_dan_kompresi(root->anak_kiri, length + 1) + total_dan_kompresi(root->anak_kanan, length + 1);
    }
}

// C. membaca Huffman tree dari file terkompresi
struct huffmanNode *baca_huffman_tree(char *namaFile){
    FILE *file = fopen(namaFile, "r");
    if (file == NULL){
        printf("Gagal membuka file\n");
        exit(1);
    }
    //membaca karakter pertama dr file
    char ch = fgetc(file);
    if (ch == EOF){
        printf("File kosong\n");
        exit(1);
    }
    //root dr huffman tree yg dibaca file
    struct huffmanNode *root = nodeBaruHuff('$', 0);
    struct huffmanNode *cur_node = root;

    while (ch != EOF){ 
        //karakter = 0, dicek ada anak kiri, akan dibuat node baru
        if (ch == '0'){
            if (cur_node->anak_kiri == NULL){
                cur_node->anak_kiri = nodeBaruHuff('$', 0);
            }
            cur_node = cur_node->anak_kiri;
        }
        //karakter = 1, cek anak kanan
        else if (ch == '1'){
            if (cur_node->anak_kanan == NULL){
                cur_node->anak_kanan = nodeBaruHuff('$', 0);
            }
            cur_node = cur_node->anak_kanan;
        }
        ch = fgetc(file);
    }
    fclose(file);
    return root;
}

// mencari kode huffman tiap huruf di binary tree dan dikompress
void cari_huffmanKode(struct huffmanNode *root, char *kode, int length_kode, char *inpFile, char *kompres){
    if (root->anak_kiri == NULL && root->anak_kanan == NULL) {
        kode[length_kode] = '\0';
        printf("%c: %s\n", root->huruf, kode);
        int i = 0;
        while (i < strlen(inpFile)){
            if (toupper(inpFile[i]) == root->huruf){
                strcat(kompres, kode);
            }
            i++;
        }
        return;
    } 
    kode[length_kode] = '0';
    cari_huffmanKode(root->anak_kiri, kode, length_kode + 1, inpFile, kompres);
    kode[length_kode] = '1';
    cari_huffmanKode(root->anak_kanan, kode, length_kode + 1, inpFile, kompres);
}


int main(int argc, char *argv[]){
    //mengecek jumlah argument 
    if (argc != 2){
        fprintf(stderr, "Usage: %s [namaFile]\n", argv[0]);
        exit(1);
    }

    char *namaFile = argv[1];
    int fd = open(namaFile, O_RDWR);
    if (fd == -1){
        perror("tidak bisa mengakses file");
        return 1;
    }

    // inisialisasi 2 pipe
    int fd1[2]; // menyimpan dua ujung pipa pertama
    int fd2[2]; // menyimpan dua ujung pipa kedua

    if (pipe(fd1) < 0 || pipe(fd2) < 0) {
        perror("Pipe Failed");
        return 1;
    }

    pid_t p;
    p = fork();

    if (p < 0){
        fprintf(stderr, "fork Failed");
        return 1;
    }

    // Parent process
    else if (p > 0){
        // Menutup pembacaan akhir pipa pertama
        close(fd1[0]); 
        // Menutup pembacaan akhir pipa kedua
        close(fd2[1]); 

        int frekuensi[CHAR_MAX] = {0};
        char huruf;
        int total_bits = 0;
        while (read(fd, &huruf, 1) > 0){
            if (isalpha(huruf)){
                huruf = toupper(huruf);
                frekuensi[(int)huruf]++;
                // Menambah 8 bit di setiap huruf yang ditemukan
                total_bits += 8;
            }
        }

        int i = 0;
        while (i < CHAR_MAX){
            if (frekuensi[i] > 0){
                if (isalpha(i)) {
                    printf("%c: %d\n", i, frekuensi[i]);
                } else {
                    printf("'%c': %d\n", i, frekuensi[i]);
                }
            }
            i++;
        }

        printf("\nSebelum kompress(bit): %d\n", total_bits);
        close(fd);

        // mengirim frekuensi ke child process
        write(fd1[1], frekuensi, sizeof(frekuensi));
        close(fd1[1]);

        // menunggu untuk mengirim bit yang dikompress
        int bit_kompres;
        read(fd2[0], &bit_kompres, sizeof(bit_kompres));
        close(fd2[0]);

        float perbandinganKompres = (float)bit_kompres / total_bits;

        return 0;
    }
    // Child process
    else{
        close(fd1[1]); // menutup tulisan ujung pipa pertama
        close(fd2[0]); // menutup tulisan ujung pipa kedua

        // membaca frekuensi dari parent process
        int frekuensi[CHAR_MAX] = {0};
        read(fd1[0], frekuensi, sizeof(frekuensi));
        close(fd1[0]);

        //mengoversi isi file ke bentuk frekuensi masing-masing karakter
        char inpFile[CHAR_MAX * 100] = "";
        int i = 0;
        while (i < CHAR_MAX){
            int j = 0;
            while (j < frekuensi[i]){
                char huruf = (char)i;
                strncat(inpFile, &huruf, 1);
                j++;
            }
            i++;
        }

        struct huffmanNode *root = buat_huffman_tree(inpFile, frekuensi);
        char kode[CHAR_MAX] = "";
        char kompres[CHAR_MAX * 100] = "";
        cari_huffmanKode(root, kode, 0, inpFile, kompres);

        int bit_kompres = strlen(kompres);
        printf("Setelah kompress(bit): %d\n", bit_kompres);
        float perbandinganKompres = (float)bit_kompres / total_dan_kompresi(root, 0);
        printf("Perbandingan: %.2f\n", perbandinganKompres);

        // mengirim hasil kompresi ke parent
        write(fd2[1], &bit_kompres, sizeof(bit_kompres));
        close(fd2[1]);
        exit(0);
    }
}
```
## Tes Output
Frekuensi Huruf
![soal1](https://i.ibb.co/D1v4gSC/image.png)

- Jumlah Bit Sebelum Dikompres
- Hasil pengubahan huruf ke Kode Huffman
- Jumlah Bit Setelah Dikompres
- Perbandingan Jumlah Bit Setelah Dikompres dan Jumlah Bit Sebelum Dikompres
![soal1](https://i.ibb.co/8BwrBR3/image.png)



# Soal 2
## Analisis Soal
- Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar. 
- Membuat program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c. Tampilkan hasil matriks tersebut ke layar dengan menerapkan konsep shared memory   
- Setelah ditampilkan, cari nilai faktorialnya untuk setiap angka dari matriks tersebut. Tampilkan hasilnya ke layar dengan format seperti matriks dengan menerapkan thread dan multithreading dalam penghitungan faktorial
- Buatlah program C ketiga dengan nama sisop.c. Program ini seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Tunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak.

## Cara Pengerjaan Soal 2

## kalian.c
- mendefinisikan matriks dan variabel indeks yang akan digunakan
```sh
int main() {
    int matriks1[4][2], matriks2[2][5], hasil[4][5], i, j, k;
```
- menginisialisasi generator angka acak dengan waktu sistem saat ini sebagai seed
```sh
srand(time(NULL));
```
- mengisi matriks pertama dengan angka acak antara 1 dan 5 (inklusif) dan mencetaknya ke konsol.
```sh
for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 5 + 1;
            printf("%d ", matriks1[i][j]);
        }
        printf("\n");
    }
```
- mengisi matriks kedua dengan angka acak antara 1 dan 4 (inklusif) dan mencetaknya ke konsol.
```sh
 for (i = 0; i < 2; i++) {
        for (j = 0; j < 5; j++) {
            matriks2[i][j] = rand() % 4 + 1;
            printf("%d ", matriks2[i][j]);
        }
        printf("\n");
    }
```
- melakukan perkalian matriks dan mencetak hasilnya ke konsol.
```sh
for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            hasil[i][j] = 0;
            for (k = 0; k < 2; k++) {
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }
```
- membuat segmen shared memory baru atau mengambil segmen yang sudah ada dengan kunci 1234 dan memasangkan segmen tersebut ke alamat memori proses.
```sh
key_t key = 1234;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix)[5] = shmat(shmid, NULL, 0);
```
- mengisi shared memory dengan hasil perkalian matriks.
```sh
for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            shared_matrix[i][j] = hasil[i][j];
        }
    }
```
- Setelah selesai mengisi shared memory, proses melepaskan (detaches) shared memory tersebut dengan fungsi shmdt()
```sh
shmdt(shared_matrix);
```
- membuat atau mengakses segmen shared memory lain dengan kunci 5678 dan menghubungkannya dengan alamat memori proses.
```sh
key_t key2 = 5678;
    int shmid2 = shmget(key2, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix2)[5] = shmat(shmid2, NULL, 0);
```
- mengisi shared memory kedua dengan hasil perkalian matriks.
```sh
for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            shared_matrix2[i][j] = hasil[i][j];
        }
    }
```
- setelah selesai mengisi shared memory, proses melepaskan shared memory tersebut.
```sh
 shmdt(shared_matrix2);
```

## cinta.c
- mendefinisikan array 2D yang akan digunakan untuk menyimpan matriks hasil perkalian yang diambil dari shared memory.
```sh
int hasil[4][5];
```
- mendefinisikan tipe data baru yaitu struct Index yang berisi dua anggota, row dan col.
```sh
typedef struct {
    int row;
    int col;
} Index;
```
- fungsi rekursif untuk menghitung faktorial dari suatu angka.
```sh
unsigned long long factorial(int n) {
    if (n == 0 || n == 1)
        return 1;
    else
        return n * factorial(n - 1);
}
```
- fungsi yang akan dijalankan oleh setiap thread. Fungsi ini mengambil argumen berupa void * yang nantinya akan di-cast ke Index *. Fungsi ini mencetak faktorial dari elemen matriks pada indeks yang ditentukan.
```sh
void *calculate_factorial(void *idx) {
    Index *index = (Index *)idx;
    printf("%llu ", factorial(hasil[index->row][index->col]));
}
```
- mengambil segmen shared memory dengan kunci 1234 dan memasangkan segmen tersebut ke alamat memori proses.
```sh
key_t key = 1234;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix)[5] = shmat(shmid, NULL, 0);
```
- mengisi array hasil dengan data dari shared memory dan mencetaknya ke konsol.
```sh
for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            hasil[i][j] = shared_matrix[i][j];
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }
```
- mencatat waktu mulai dan membuat thread untuk setiap elemen
```sh
clock_t start = clock();
```
- menciptakan sebuah thread untuk setiap elemen pada matriks. Setiap thread akan menjalankan fungsi calculate_factorial dengan argument berupa pointer ke struktur Index yang berisi indeks baris dan kolom dari elemen yang akan dihitung faktorialnya.
```sh
pthread_t threads[4][5];
for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            Index *index = malloc(sizeof(Index));
            index->row = i;
            index->col = j;
            pthread_create(&threads[i][j], NULL, calculate_factorial, (void *)index);
        }
    }
```
- Setelah semua thread dibuat, program akan menunggu setiap thread selesai menjalankan tugasnya dengan menggunakan fungsi pthread_join(). Setelah semua thread pada baris matriks selesai, program akan mencetak baris baru.
```sh
for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            pthread_join(threads[i][j], NULL);
        }
        printf("\n");
    }

```
- Setelah semua perhitungan faktorial selesai, program mencatat waktu selesai dan menghitung durasi yang dibutuhkan untuk melakukan semua perhitungan tersebut. Durasi ini kemudian dicetak ke konsol.
```sh
clock_t end = clock();
    double time_taken = (double)(end - start) / CLOCKS_PER_SEC;
    printf("\nWaktu yang dibutuhkan dengan multithreading: %f detik\n", time_taken);
```
- Setelah selesai, program akan melepaskan segmen shared memory dari alamat memori proses dan kemudian menghapus segmen shared memory tersebut.
```sh
shmdt(shared_matrix);
    shmctl(shmid, IPC_RMID, NULL);
```

## sisop.c
- sama seperti cinta.c tetapi hanya ada beberapa code saja yang diubah
```sh
void calculate_factorial_without_thread(int i, int j) {
    printf("%llu ", factorial(hasil[i][j]));
}
```
- mengambil segmen shared memory dengan kunci 5678 dan memasangkan segmen tersebut ke alamat memori proses.
```sh
key_t key2 = 5678;
    int shmid2 = shmget(key2, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix2)[5] = shmat(shmid2, NULL, 0);
```
- mencatat waktu mulai dan melakukan perhitungan faktorial untuk setiap elemen pada matriks tanpa menggunakan thread.
```sh
clock_t start = clock();
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            calculate_factorial_without_thread(i, j);
        }
        printf("\n");
    }
```
- Meskipun multithreading secara teori seharusnya dapat meningkatkan kecepatan eksekusi program dengan memanfaatkan paralelisme pada komputer multi-core. Secara keseluruhan, manfaat sebenarnya dari multithreading biasanya hanya terlihat ketika tugas yang dilakukan oleh setiap thread cukup besar sehingga manfaat dari paralelisme melebihi overhead dari multithreading itu sendiri. Dalam kasus ini, perhitungan faktorial mungkin tidak cukup "berat" untuk membenarkan penggunaan multithreading.

## Source Code
## kalian.c
```sh
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>

int main() {
    int matriks1[4][2], matriks2[2][5], hasil[4][5], i, j, k;

    srand(time(NULL));
    
    printf("Matriks Pertama:\n");
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 5 + 1;
            printf("%d ", matriks1[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriks Kedua:\n");
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 5; j++) {
            matriks2[i][j] = rand() % 4 + 1;
            printf("%d ", matriks2[i][j]);
        }
        printf("\n");
    }

    printf("\nHasil Perkalian Matriks:\n");
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            hasil[i][j] = 0;
            for (k = 0; k < 2; k++) {
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix)[5] = shmat(shmid, NULL, 0);

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            shared_matrix[i][j] = hasil[i][j];
        }
    }

    shmdt(shared_matrix);

    key_t key2 = 5678;
    int shmid2 = shmget(key2, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix2)[5] = shmat(shmid2, NULL, 0);

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            shared_matrix2[i][j] = hasil[i][j];
        }
    }

    shmdt(shared_matrix2);

    return 0;
}
```
## cinta.c
```sh
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>

int hasil[4][5];

typedef struct {
    int row;
    int col;
} Index;

unsigned long long factorial(int n) {
    if (n == 0 || n == 1)
        return 1;
    else
        return n * factorial(n - 1);
}

void *calculate_factorial(void *idx) {
    Index *index = (Index *)idx;
    printf("%llu ", factorial(hasil[index->row][index->col]));
}

int main() {

    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix)[5] = shmat(shmid, NULL, 0);
    
    printf("Matriks Hasil Perkalian:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            hasil[i][j] = shared_matrix[i][j];
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriks Faktorial:\n");
    clock_t start = clock();
    pthread_t threads[4][5];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            Index *index = malloc(sizeof(Index));
            index->row = i;
            index->col = j;
            pthread_create(&threads[i][j], NULL, calculate_factorial, (void *)index);
        }
    }

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            pthread_join(threads[i][j], NULL);
        }
        printf("\n");
    }

    clock_t end = clock();
    double time_taken = (double)(end - start) / CLOCKS_PER_SEC;
    printf("\nWaktu yang dibutuhkan dengan multithreading: %f detik\n", time_taken);

    shmdt(shared_matrix);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}

```
## sisop.c
```sh
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>

int hasil[4][5];

typedef struct {
    int row;
    int col;
} Index;

unsigned long long factorial(int n) {
    if (n == 0 || n == 1)
        return 1;
    else
        return n * factorial(n - 1);
}

void calculate_factorial_without_thread(int i, int j) {
    printf("%llu ", factorial(hasil[i][j]));
}

int main() {
    key_t key2 = 5678;
    int shmid2 = shmget(key2, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix2)[5] = shmat(shmid2, NULL, 0);
    
    printf("Matriks Hasil Perkalian:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            hasil[i][j] = shared_matrix2[i][j];
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriks Faktorial:\n");
    clock_t start = clock();
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            calculate_factorial_without_thread(i, j);
        }
        printf("\n");
    }
 
    clock_t end = clock();
    double time_taken = (double)(end - start) / CLOCKS_PER_SEC;
    printf("\nWaktu yang dibutuhkan tanpa multithreading: %f detik\n", time_taken);

    shmdt(shared_matrix2);
    shmctl(shmid2, IPC_RMID, NULL);

    return 0;
}

```

## Tes Output
Output matriks 4x2 dan 2x5 dan juga hasil perkaliannya pada kalian.c    
![soal2](https://i.ibb.co/Mc5qZFc/image.png)

Output cinta.c yang sudah sesuai dengan kalian.c dengan multithreading  
![soal2](https://i.ibb.co/QnKwDJb/image.png)

Output sisop.c yang sudah sesuai dengan kalian.c tanpa multithreading   
![soal2](https://i.ibb.co/vm4bGw9/image.png)

Output perbandingan kecepatan antara cinta.c dengan sisop.c     
![soal2](https://i.ibb.co/vm4bGw9/image.png)


# Soal 3
## Analisis Soal
- Membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem. 
- User dapat mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
- Perintah DECRYPT akan melakukan decrypt/decode/konversi dengan metode ROT13, Base64, dan Hex
Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket 
- User dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt
- User juga dapat mengirimkan perintah PLAY <SONG>

## Cara Pengerjaan Soal 2

## stream.c
- Mendefinisikan nama antrian pesan.
```sh
#define QUEUE_NAME "/message_queue"
```
- Mendefinisikan izin file dengan mode baca/tulis untuk pengguna, grup, dan orang lain.
```sh
#define PERMISSIONS 0666
```
- Mendefinisikan ukuran buffer untuk menerima pesan.
```sh
#define BUFFER_SIZE (MAX_MSG_SIZE + 10)
```
- membandingkan dua string dengan tidak memperhatikan case (huruf besar atau kecil). Fungsi ini digunakan oleh qsort() untuk mengurutkan playlist.
```sh
int compare_str(const void *a, const void *b) {
    const char *str_a = *(const char **)a;
    const char *str_b = *(const char **)b;
    return strcasecmp(str_a, str_b);
}
```
- mengurutkan playlist dan menulisnya ke file output.
```sh
void sort_and_write_playlist(const char **playlist, size_t song_count, const char *output_file) {
    qsort(playlist, song_count, sizeof(char *), compare_str);

    FILE *out = fopen(output_file, "w");
    if (out == NULL) {
        perror("fopen");
        exit(1);
    }

    for (size_t i = 0; i < song_count; i++) {
        fprintf(out, "%s\n", playlist[i]);
    }

    fclose(out);
}
```
- Fungsi-fungsi decrypt. Mendekripsi string dengan algoritma ROT13.
```sh
void rot13(char *str) {
    for (int i = 0; str[i]; i++) {
        char c = str[i];
        if (c >= 'a' && c <= 'z') {
            str[i] = ((c - 'a' + 13) % 26) + 'a';
        } else if (c >= 'A' && c <= 'Z') {
            str[i] = ((c - 'A' + 13) % 26) + 'A';
        }
    }
}
```
- mendekripsi string yang dienkripsi dengan Base64.
```sh
void base64_decode(const char *src, char *dst) {
    char command[512];
    snprintf(command, sizeof(command), "echo '%s' | base64 -d > decoded.txt", src);
    system(command);

    FILE *file = fopen("decoded.txt", "r");
    if (file == NULL) {
        perror("fopen");
        exit(1);
    }
    fgets(dst, 256, file);
    fclose(file);
    remove("decoded.txt");
}
```
- mendekripsi string yang dienkripsi dalam bentuk hexadecimal.
```sh
void hex_decode(const char *src, char *dst) {
    size_t len = strlen(src);
    for (size_t i = 0; i < len; i += 2) {
        unsigned int byte;
        sscanf(src + i, "%2x", &byte);
        dst[i / 2] = byte;
    }
    dst[len / 2] = '\0';
}
```
- membaca file JSON yang berisi daftar lagu yang dienkripsi, mendekripsinya, dan menulis hasilnya ke file output.
```sh
void decrypt_json(const char *input_file, const char *output_file) {
    json_object *root, *song_entry;
    enum json_tokener_error error = json_tokener_success;

    FILE *fp = fopen(input_file, "r");
    if (fp == NULL) {
        perror("fopen");
        exit(1);
    }

    fseek(fp, 0, SEEK_END);
    size_t file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char *file_content = malloc(file_size + 1);
    if (file_content == NULL) {
        perror("malloc");
        exit(1);
    }
    fread(file_content, 1, file_size, fp);
    fclose(fp);

    root = json_tokener_parse_verbose(file_content, &error);
    if (error != json_tokener_success) {
        fprintf(stderr, "Error parsing JSON: %s\n", json_tokener_error_desc(error));
        exit(1);
    }

    FILE *out = fopen(output_file, "w");
    if (out == NULL) {
        perror("fopen");
        exit(1);
    }

    size_t song_count = json_object_array_length(root);
    const char **decrypted_songs = malloc(song_count * sizeof(char *));
    if (decrypted_songs == NULL) {
        perror("malloc");
        exit(1);
    }

    for (size_t i = 0; i < song_count; i++) {
        song_entry = json_object_array_get_idx(root, i);
        const char *method = json_object_get_string(json_object_object_get(song_entry, "method"));
        const char *song = json_object_get_string(json_object_object_get(song_entry, "song"));

        char *decrypted_song = malloc(MAX_MSG_SIZE);
        if (decrypted_song == NULL) {
            perror("malloc");
            exit(1);
        }
        strcpy(decrypted_song, song);

        if (strcmp(method, "rot13") == 0) {
            rot13(decrypted_song);
        } else if (strcmp(method, "base64") == 0) {
            base64_decode(song, decrypted_song);
        } else if (strcmp(method, "hex") == 0) {
            hex_decode(song, decrypted_song);
        } else {
            fprintf(stderr, "Unknown decryption method: %s\n", method);
            exit(1);
        }

        decrypted_songs[i] = decrypted_song;
    }

    sort_and_write_playlist(decrypted_songs, song_count, output_file);

    for (size_t i = 0; i < song_count; i++) {
        free((void *)decrypted_songs[i]);
    }
    free(decrypted_songs);
    json_object_put(root);
    free(file_content);
}
```
- membaca file output dan mencetak setiap lagu. Ini digunakan untuk menampilkan daftar lagu.
```sh
void list_songs(const char *output_file) {
    FILE *fp = fopen(output_file, "r");
    if (fp == NULL) {
        perror("fopen");
        exit(1);
    }

    char line[256];
    while (fgets(line, sizeof(line), fp)) {
        printf("%s", line);
    }

    fclose(fp);
}
```
- mencari lagu yang diberikan dalam file playlist dan mencetaknya. Jika lagu tidak ditemukan, fungsi ini mencetak pesan bahwa lagu tersebut tidak ada.
```sh
 void play_song(const char *song_title, const char *playlist_file) {
    FILE *fp = fopen(playlist_file, "r");
    if (fp == NULL) {
        perror("fopen");
        exit(1);
    }

    char line[256];
    int song_count = 0;
    bool found = false;
    while (fgets(line, sizeof(line), fp)) {
        line[strcspn(line, "\n")] = '\0';
        if (strcasestr(line, song_title) != NULL) {
            found = true;
            song_count++;
            printf("%d. %s\n", song_count, line);
        }
    }

    fclose(fp);

    if (!found) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song_title);
    } else {
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", song_count, song_title);
    }
}
```
- mendefinisikan buffer untuk pesan dan mengatur atribut-antribut untuk message queue yang akan dibuat
```sh
char buffer[BUFFER_SIZE];

    attr.mq_flags = 0;
    attr.mq_maxmsg = MAX_MESSAGES;
    attr.mq_msgsize = MAX_MSG_SIZE;
    attr.mq_curmsgs = 0;
```
- membuka atau membuat (jika belum ada) sebuah message queue dengan nama QUEUE_NAME, mode hanya-baca, dan permissions yang telah ditentukan.
```sh
mq = mq_open(QUEUE_NAME, O_CREAT | O_RDONLY, PERMISSIONS, &attr);
    if (mq == -1) {
        perror("mq_open");
        exit(1);
    }
```
- membaca pesan dari message queue dalam loop tak-terbatas. 
```sh
while (1) {
        ssize_t bytes_read;
        bytes_read = mq_receive(mq, buffer, BUFFER_SIZE, NULL);
        if (bytes_read <= 0) {
            perror("mq_receive");
            exit(1);
        }
        buffer[bytes_read] = '\0';
```
- mengecek isi pesan dan melakukan aksi sesuai pesan tersebut
```sh
if (strcmp(buffer, "DECRYPT") == 0) {
            decrypt_json("song-playlist.json", "playlist.txt");
        } else if (strcmp(buffer, "LIST") == 0) {
            list_songs("playlist.txt");
        } else if (strncmp(buffer, "PLAY ", 5) == 0) {
            const char *song_title = buffer + 5;
            play_song(song_title, "playlist.txt");
        } else {
            printf("Invalid command: %s\n", buffer);
        }
```

## user.c
- Mendeklarasikan variabel-variabel yang diperlukan: mq untuk menyimpan descriptor dari message queue, attr untuk menyimpan atribut-atribut dari message queue, dan buffer untuk menyimpan pesan yang akan dikirim ke message queue.
```sh
mqd_t mq;
    struct mq_attr attr;
    char buffer[MAX_MSG_SIZE];
```
- Menginisialisasi struktur attr dengan nilai-nilai default.
```sh
attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MAX_MSG_SIZE;
    attr.mq_curmsgs = 0;
```
- membuka message queue dengan nama yang didefinisikan sebelumnya dan mode "write only", lalu menyimpan descriptor-nya ke dalam variabel mq.
```sh
mq = mq_open(QUEUE_NAME, O_WRONLY);
    if (mq == -1) {
        perror("mq_open");
        exit(1);
    }
```
- Membaca perintah dari user dan menyimpannya ke dalam buffer.
```sh
fgets(buffer, MAX_MSG_SIZE, stdin);
```
- Menghilangkan karakter newline dari akhir buffer.
```sh
buffer[strcspn(buffer, "\n")] = '\0';

        if (strcmp(buffer, "QUIT") == 0) {
            break;
        } else if (strncmp(buffer, "PLAY ", 5) == 0) {
            const char *song_title = buffer + 5;
            printf("USER %d PLAYING \"%s\"\n", getpid(), song_title);
        }

        if (mq_send(mq, buffer, strlen(buffer), 0) == -1) {
            perror("mq_send");
            exit(1);
        }

```
- Setelah itu compile stream.c dengan perintah `gcc stream.c -o stream -ljson-c` karena kode tersebut menggunakan library json. Compile juga untuk user.c seperti biasa, setelah itu run `./stream` terlebih dahulu lalu run `./user`. Di sini saya menggunakan PID untuk identifier user nya

## Source Code
## stream.c
```sh
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mqueue.h>
#include <sys/stat.h>
#include <json-c/json.h>
#include <ctype.h>
#include <stdbool.h>

#define QUEUE_NAME "/message_queue"

#define PERMISSIONS 0666
#define MAX_MESSAGES 10
#define MAX_MSG_SIZE 256

#define BUFFER_SIZE (MAX_MSG_SIZE + 10)

int compare_str(const void *a, const void *b) {
    const char *str_a = *(const char **)a;
    const char *str_b = *(const char **)b;
    return strcasecmp(str_a, str_b);
}

void sort_and_write_playlist(const char **playlist, size_t song_count, const char *output_file) {
    qsort(playlist, song_count, sizeof(char *), compare_str);

    FILE *out = fopen(output_file, "w");
    if (out == NULL) {
        perror("fopen");
        exit(1);
    }

    for (size_t i = 0; i < song_count; i++) {
        fprintf(out, "%s\n", playlist[i]);
    }

    fclose(out);
}

void rot13(char *str) {
    for (int i = 0; str[i]; i++) {
        char c = str[i];
        if (c >= 'a' && c <= 'z') {
            str[i] = ((c - 'a' + 13) % 26) + 'a';
        } else if (c >= 'A' && c <= 'Z') {
            str[i] = ((c - 'A' + 13) % 26) + 'A';
        }
    }
}

void base64_decode(const char *src, char *dst) {
    char command[512];
    snprintf(command, sizeof(command), "echo '%s' | base64 -d > decoded.txt", src);
    system(command);

    FILE *file = fopen("decoded.txt", "r");
    if (file == NULL) {
        perror("fopen");
        exit(1);
    }
    fgets(dst, 256, file);
    fclose(file);
    remove("decoded.txt");
}

void hex_decode(const char *src, char *dst) {
    size_t len = strlen(src);
    for (size_t i = 0; i < len; i += 2) {
        unsigned int byte;
        sscanf(src + i, "%2x", &byte);
        dst[i / 2] = byte;
    }
    dst[len / 2] = '\0';
}

void decrypt_json(const char *input_file, const char *output_file) {
    json_object *root, *song_entry;
    enum json_tokener_error error = json_tokener_success;

    FILE *fp = fopen(input_file, "r");
    if (fp == NULL) {
        perror("fopen");
        exit(1);
    }

    fseek(fp, 0, SEEK_END);
    size_t file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char *file_content = malloc(file_size + 1);
    if (file_content == NULL) {
        perror("malloc");
        exit(1);
    }
    fread(file_content, 1, file_size, fp);
    fclose(fp);

    root = json_tokener_parse_verbose(file_content, &error);
    if (error != json_tokener_success) {
        fprintf(stderr, "Error parsing JSON: %s\n", json_tokener_error_desc(error));
        exit(1);
    }

    FILE *out = fopen(output_file, "w");
    if (out == NULL) {
        perror("fopen");
        exit(1);
    }

    size_t song_count = json_object_array_length(root);
    const char **decrypted_songs = malloc(song_count * sizeof(char *));
    if (decrypted_songs == NULL) {
        perror("malloc");
        exit(1);
    }

    for (size_t i = 0; i < song_count; i++) {
        song_entry = json_object_array_get_idx(root, i);
        const char *method = json_object_get_string(json_object_object_get(song_entry, "method"));
        const char *song = json_object_get_string(json_object_object_get(song_entry, "song"));

        char *decrypted_song = malloc(MAX_MSG_SIZE);
        if (decrypted_song == NULL) {
            perror("malloc");
            exit(1);
        }
        strcpy(decrypted_song, song);

        if (strcmp(method, "rot13") == 0) {
            rot13(decrypted_song);
        } else if (strcmp(method, "base64") == 0) {
            base64_decode(song, decrypted_song);
        } else if (strcmp(method, "hex") == 0) {
            hex_decode(song, decrypted_song);
        } else {
            fprintf(stderr, "Unknown decryption method: %s\n", method);
            exit(1);
        }

        decrypted_songs[i] = decrypted_song;
    }

    sort_and_write_playlist(decrypted_songs, song_count, output_file);

    for (size_t i = 0; i < song_count; i++) {
        free((void *)decrypted_songs[i]);
    }
    free(decrypted_songs);
    json_object_put(root);
    free(file_content);
}

void list_songs(const char *output_file) {
    FILE *fp = fopen(output_file, "r");
    if (fp == NULL) {
        perror("fopen");
        exit(1);
    }

    char line[256];
    while (fgets(line, sizeof(line), fp)) {
        printf("%s", line);
    }

    fclose(fp);
}

void play_song(const char *song_title, const char *playlist_file) {
    FILE *fp = fopen(playlist_file, "r");
    if (fp == NULL) {
        perror("fopen");
        exit(1);
    }

    char line[256];
    int song_count = 0;
    bool found = false;
    while (fgets(line, sizeof(line), fp)) {
        line[strcspn(line, "\n")] = '\0';
        if (strcasestr(line, song_title) != NULL) {
            found = true;
            song_count++;
            printf("%d. %s\n", song_count, line);
        }
    }

    fclose(fp);

    if (!found) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song_title);
    } else {
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", song_count, song_title);
    }
}

int main() {
    mqd_t mq;
    struct mq_attr attr;

    char buffer[BUFFER_SIZE];

    attr.mq_flags = 0;
    attr.mq_maxmsg = MAX_MESSAGES;
    attr.mq_msgsize = MAX_MSG_SIZE;
    attr.mq_curmsgs = 0;

    mq = mq_open(QUEUE_NAME, O_CREAT | O_RDONLY, PERMISSIONS, &attr);
    if (mq == -1) {
        perror("mq_open");
        exit(1);
    }

    while (1) {
        ssize_t bytes_read;
        bytes_read = mq_receive(mq, buffer, BUFFER_SIZE, NULL);
        if (bytes_read <= 0) {
            perror("mq_receive");
            exit(1);
        }
        buffer[bytes_read] = '\0';

        if (strcmp(buffer, "DECRYPT") == 0) {
            decrypt_json("song-playlist.json", "playlist.txt");
        } else if (strcmp(buffer, "LIST") == 0) {
            list_songs("playlist.txt");
        } else if (strncmp(buffer, "PLAY ", 5) == 0) {
            const char *song_title = buffer + 5;
            play_song(song_title, "playlist.txt");
        } else {
            printf("Invalid command: %s\n", buffer);
        }
    }

    mq_close(mq);
    mq_unlink(QUEUE_NAME);

    return 0;
}
```

## user.c
```sh
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mqueue.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define QUEUE_NAME "/message_queue"
#define PERMISSIONS 0666
#define MAX_MSG_SIZE 256

int main() {
    mqd_t mq;
    struct mq_attr attr;
    char buffer[MAX_MSG_SIZE];

    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MAX_MSG_SIZE;
    attr.mq_curmsgs = 0;

    mq = mq_open(QUEUE_NAME, O_WRONLY);
    if (mq == -1) {
        perror("mq_open");
        exit(1);
    }

    while (1) {
        printf("Enter command (DECRYPT, LIST, PLAY, or QUIT): ");

        fgets(buffer, MAX_MSG_SIZE, stdin);

        buffer[strcspn(buffer, "\n")] = '\0';

        if (strcmp(buffer, "QUIT") == 0) {
            break;
        } else if (strncmp(buffer, "PLAY ", 5) == 0) {
            const char *song_title = buffer + 5;
            printf("USER %d PLAYING \"%s\"\n", getpid(), song_title);
        }

        if (mq_send(mq, buffer, strlen(buffer), 0) == -1) {
            perror("mq_send");
            exit(1);
        }
    }

    mq_close(mq);

    return 0;
}
```

## Tes Output
Output file txt yang sudah di decrypt dan diurutkan sesuai abjad    
![soal3](https://i.ibb.co/LtN6m6d/image.png)

Output perintah LIST dari user.c    
![soal3](https://i.ibb.co/qRR919S/image.png)

Output perintah PLAY <SONG> dari user.c 
![soal3](https://i.ibb.co/b6FBSFL/image.png)

# Soal 4
## Analisis Soal
- Membuat unzip.c untuk mendownload sekaligus meng-ekstrak file yang terdapat dalam hehe.zip.
- Membuat Categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya.
- Membuat suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt

## Cara Pengerjaan
- Buatlah program unzip.C yang melakukan tiga tugas: mengunduh file dari URL, mengekstrak file zip, dan menghapus file yang diunduh Program ini menggunakan konsep fork-exec untuk membuat proses anak yang menjalankan perintah shell seperti wget, unzip, dan rm. Dengan menggunakan execv(), program dapat menggantikan dirinya sendiri dengan perintah shell yang dijalankan oleh proses anak. Setelah itu, proses induk menunggu proses anak selesai sebelum melanjutkan eksekusi program.
```bash
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <math.h>
#include <pthread.h>

int main() {
    char* url = "https://drive.google.com/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
    char* downloadName = "hehe.zip";
    int status;
    pid_t child;

    child = fork();
    if (child == 0) {
        char* args[] = {"wget", "-O", downloadName, url, NULL};
        execv("/bin/wget", args);
    }
    waitpid(child, &status, 0);

    child = fork();
    if (child == 0) {
        char* args[] = {"unzip", downloadName, NULL};
        execv("/bin/unzip", args);
    }
    waitpid(child, &status, 0);

    child = fork();
    if (child == 0) {
        char* args[] = {"rm", downloadName, NULL};
        execv("/bin/rm", args);
    }
    waitpid(child, &status, 0);
}


```
- Butlah program categorize.c 

1. Program ini menggunakan beberapa header file standar seperti stdio.h, stdlib.h, string.h, dan lain-lain.

2. Program ini mendefinisikan beberapa konstanta seperti MAX_EXTENSION dan beberapa string konstan seperti EXTENSIONS_FILE, MAX_FILE, LOG_FILE, CATEGORY_FOLDER, dan DIRECTORY.

3. Program ini menggunakan variabel global extension_file_num untuk menyimpan jumlah file dengan ekstensi tertentu, dan other_extension_num untuk menyimpan jumlah file dengan ekstensi "other".

4. Ada struktur data ThreadArgument yang digunakan sebagai argumen untuk thread yang akan dibuat.

5. Terdapat beberapa fungsi seperti fileProcessing, directoryProcessing, createDir, move, timeFormat, accessLog, moveLog, madeLog, directory_process_routine, dan printExtfilecount. Setiap fungsi memiliki tugas spesifik dalam pemrosesan file dan direktori.

6. Fungsi main adalah fungsi utama yang akan dieksekusi saat program dimulai. Pada fungsi main, program membaca daftar ekstensi dari file extensions.txt dan membaca nilai maksimum ekstensi dari file max.txt.

7. Program melakukan pemrosesan direktori menggunakan fungsi directoryProcessing. Fungsi ini akan membaca semua file dalam direktori yang ditentukan dan memprosesnya menggunakan fungsi fileProcessing.

8. Fungsi fileProcessing mengkategorikan file berdasarkan ekstensinya. Jika ekstensi file ditemukan dalam daftar ekstensi yang dibaca dari extensions.txt dan jumlah file dengan ekstensi tersebut belum mencapai batas maksimum, file akan dipindahkan ke direktori yang sesuai dengan ekstensi tersebut. Jika ekstensi file tidak ditemukan dalam daftar ekstensi, file akan dipindahkan ke direktori "other".

9. Program juga melakukan logging ke file log.txt dengan menggunakan fungsi-fungsi seperti accessLog, moveLog, dan madeLog.

10. Setelah pemrosesan selesai, program mencetak jumlah file untuk setiap ekstensi dalam urutan menaik menggunakan fungsi printExtfilecount.
```bash
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <math.h>
#include <pthread.h>

#define MAX_EXTENSION 100

char* EXTENSIONS_FILE = "extensions.txt";
char* MAX_FILE = "max.txt";
char* LOG_FILE = "log.txt";
char* CATEGORY_FOLDER = "categorized";
char* DIRECTORY = "files";
int* extension_file_num;
int other_extension_num;

typedef struct ThreadArgument {
    char* dirname;
    char** extensions;
    int count_extension;
    int max;
} ThreadArgument;

void fileProcessing(char* path, int num_extension, char** extensions, int max);
void directoryProcessing(char* dirname, int num_extension, char** extensions, int max);
void* directory_process_routine(void* arg);
void printExtfilecount(int ext_file_num[], char** extensions, int n_ext);

void createDir(char* dirname);
void move(char* src, char* dest);
char* timeFormat();

void accessLog(char* path);
void moveLog(char* ext, char* src, char* dest);
void madeLog(char* folder);


int main() {
    extension_file_num = calloc(MAX_EXTENSION, sizeof(int));
    other_extension_num = 0;
    char *extensions[MAX_EXTENSION], c;
    int num_extension = 0;
    int pos = 0;
    int ext_max;

    FILE* file = fopen(EXTENSIONS_FILE, "a");
    fprintf(file, "other\n");
    fclose(file);

    file = fopen(EXTENSIONS_FILE, "r");

    char line[MAX_EXTENSION];
    while (fgets(line, MAX_EXTENSION, file)) {
        char* token = strtok(line, " \t\r\n");
        while (token != NULL) {
            token[strcspn(token, "\n")] = 0;
            extensions[num_extension] = strdup(token);
            num_extension++;
            token = strtok(NULL, " \t\r\n");
        }
    }

    file = fopen(MAX_FILE, "r");
    fscanf(file, "%d", &ext_max);
    fclose(file);

    directoryProcessing(DIRECTORY, num_extension, extensions, ext_max);
    printExtfilecount(extension_file_num, extensions, num_extension);

    return 0;
}

void createDir(char* dirname) {
    char command[1000];
    sprintf(command, "mkdir %s", dirname);
    system(command);
}

void move(char* src, char* dest) {
    char command[1000];
    sprintf(command, "mv %s %s", src, dest);
    system(command);
}

char* timeFormat() {
    time_t current_time = time(NULL);
    struct tm* time_info = localtime(&current_time);

    char* formatted_time = malloc(20 * sizeof(char));
    strftime(formatted_time, 20, "%Y-%m-%d %H:%M:%S", time_info);
    return formatted_time;
}

void accessLog(char* path) {
    FILE* logFile = fopen(LOG_FILE, "a");
    fprintf(logFile, "%s ACCESSED %s\n", timeFormat(), path);
    fclose(logFile);
}

void moveLog(char* ext, char* src, char* dest) {
    FILE* logFile = fopen(LOG_FILE, "a");
    fprintf(logFile, "%s MOVED %s file : %s > %s\n", timeFormat(), ext, src, dest);
    fclose(logFile);
}

void madeLog(char* folder) {
    FILE* logFile = fopen(LOG_FILE, "a");
    fprintf(logFile, "%s MADE %s\n", timeFormat(), folder);
    fclose(logFile);
}

void fileProcessing(char* path, int num_extension, char** extensions, int max) {
    if (access("categorized", F_OK) != 0) {
        createDir("categorized");
        madeLog("categorized");
    }

    char* filename = basename(path);
    char* lastDot = strrchr(filename, '.');
    char command[1000];

    if (lastDot) {
        *lastDot = '\0';

        char ext[150], ext2[150];
        strcpy(ext, lastDot + 1);
        strcpy(ext2, ext);
        for (int i = 0; ext2[i]; i++) ext2[i] = tolower(ext[i]);

        int ext_idx = -1;
        for (int i = 0; i < num_extension; i++) {
            if (!strcmp(ext2, extensions[i])) {
                ext_idx = i;
                extension_file_num[ext_idx]++;
                break;
            }
        }

        char newPath[200], oldPath[200];

        if (ext_idx >= 0 && extension_file_num[ext_idx] <= max) {
            sprintf(newPath, "categorized/%s", extensions[ext_idx]);
        } else if (ext_idx >= 0) {  
            sprintf(newPath, "categorized/%s (%d)", extensions[ext_idx], (int)ceil(extension_file_num[ext_idx] / (float)max));
        } else {  
            extension_file_num[num_extension - 1]++;
            sprintf(newPath, "categorized/other");
        }

        if (access(newPath, F_OK) != 0) {
            sprintf(command, "mkdir \"%s\"", newPath);
            system(command);
            madeLog(newPath);
        }

        snprintf(oldPath, sizeof(oldPath), "%s.%s", path, ext);
        sprintf(command, "mv '%s' \"%s\"", oldPath, newPath);
        system(command);
        moveLog(ext, oldPath, newPath);
    } else {  
        extension_file_num[num_extension - 1]++;
        if (access("categorized/other", F_OK) != 0) {
            system("mkdir categorized/other");
            madeLog("categorized/other");
        }

        sprintf(command, "mv '%s' categorized/other", path);
        system(command);
        moveLog("other", path, "categorized/other");
    }
}

void directoryProcessing(char* dirname, int num_extension, char** extensions, int max) {
    pthread_t myThreads[200];
    int n_thread = 0;

    DIR* dir;
    struct dirent* entry;
    dir = opendir(dirname);
    accessLog(dirname);

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }
        char path[2000];
        sprintf(path, "%s/%s", dirname, entry->d_name);

        if (entry->d_type == DT_DIR) {
            struct ThreadArgument* threadArg = malloc(sizeof(struct ThreadArgument));
            threadArg->dirname = strdup(path);
            threadArg->count_extension = num_extension;
            threadArg->extensions = extensions;
            threadArg->max = max;

            pthread_create(&myThreads[n_thread], NULL, directory_process_routine, threadArg);
            n_thread++;
        } else {  
            fileProcessing(path, num_extension, extensions, max);
        }
    }
    closedir(dir);

    for (int i = 0; i < n_thread; i++) {
        pthread_join(myThreads[i], NULL);
    }
}


void* directory_process_routine(void* arg) {
    struct ThreadArgument* threadArg = (struct ThreadArgument*)arg;
    directoryProcessing(threadArg->dirname, threadArg->count_extension, threadArg->extensions, threadArg->max);
    pthread_exit(NULL);
}

void printExtfilecount(int ext_file_num[], char** extensions, int n_ext) {

    for (int i = 0; i < n_ext - 1; i++) {
        for (int j = i + 1; j < n_ext; j++) {
            if (ext_file_num[i] > ext_file_num[j]) {
                int temp = ext_file_num[i];
                ext_file_num[i] = ext_file_num[j];
                ext_file_num[j] = temp;

                char temp2[10];
                strcpy(temp2, extensions[i]);
                strcpy(extensions[i], extensions[j]);
                strcpy(extensions[j], temp2);
            }
        }
    }
    for (int i = 0; i < n_ext; i++) {
        printf("%s : %d\n", extensions[i], ext_file_num[i]);
    }
}

```
- Buatlah program logchecker.c 
Program ini adalah bagian kedua dari program yang berfungsi untuk mengolah dan menganalisis log file yang dihasilkan oleh program pertama. Program ini membaca file log yang disimpan dalam file "log.txt" dan melakukan beberapa operasi analisis.

1. extensionRead: Fungsi ini membaca daftar ekstensi yang ada dalam file log. Ia membaca ekstensi dari direktori "categorized" yang terdapat dalam file log, kemudian menyimpannya dalam array extensions.

2. countAccessed: Fungsi ini menghitung jumlah akses yang tercatat dalam file log. Ia mencari kata "ACCESSED" dalam setiap baris log dan menghitung jumlah baris yang mengandung kata tersebut.

3. countListFolder: Fungsi ini menghitung dan mencetak daftar folder dalam direktori "categorized" beserta jumlah file yang terkait dengan setiap folder tersebut. Ia menggunakan perintah grep dan cut untuk memfilter dan memotong baris log yang relevan, kemudian menggunakan perintah grep -c untuk menghitung jumlah file yang tercatat dalam setiap folder.

4. listExtension: Fungsi ini mencetak daftar ekstensi beserta jumlah file yang terkait dengan setiap ekstensi. Ia menggunakan perintah grep -c untuk menghitung jumlah file yang terkait dengan setiap ekstensi dalam file log.

5. Program ini digunakan sebagai bagian kedua dari program yang lebih besar yang bertujuan untuk mengkategorikan file berdasarkan ekstensinya. Program ini membantu menganalisis log file yang dihasilkan oleh program pertama untuk memberikan informasi tentang jumlah akses, daftar folder, dan daftar ekstensi yang relevan.
```bash
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <math.h>
#include <pthread.h>

#define MAX_EXTENSION 100
#define MAX_LINE_LENGTH 100

char* LOG_FILE = "log.txt";
int* extension_file_num;

int extensionRead(char** extensions);
int countAcessed();
void listExtension(int extension_file_num[], char** extensions, int num_extension);
void countListFolder();

int main() {
    extension_file_num = calloc(MAX_EXTENSION, sizeof(int));
    char* extensions[MAX_EXTENSION];
    int num_extension = extensionRead(extensions);

    printf("ACCESSED %d times\n", countAcessed());
    printf("----------------------------------\n");
    countListFolder();
    printf("----------------------------------\n");
    listExtension(extension_file_num, extensions, num_extension);

    return 0;
}

int extensionRead(char** extensions) {
    char cmd[MAX_LINE_LENGTH];
    char line[MAX_LINE_LENGTH];
    int num_extension = 0;

    sprintf(cmd, "grep -oP '(?<=categorized/)[^\\s]*' %s | sort -u", LOG_FILE);

    FILE* fp = popen(cmd, "r");

    if (fp == NULL) {
        printf("Failed to execute command\n");
        return 1;
    }

    while (fgets(line, MAX_LINE_LENGTH, fp) != NULL) {
        line[strcspn(line, "\n")] = 0;
        extensions[num_extension] = malloc(strlen(line) + 1);
        strcpy(extensions[num_extension], line);
        num_extension++;
    }
    pclose(fp);
    return num_extension;
}

int countAcessed() {
    char accessed[] = "ACCESSED";
    char line[100];
    int count = 0;

    FILE* file = fopen(LOG_FILE, "r");
    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, accessed)) {
            count++;
        }
    }
    fclose(file);
    return count;
}

void countListFolder() {
    char cmd[MAX_LINE_LENGTH];
    char line[MAX_LINE_LENGTH];
    int i, j, numFolder = 0;

    char* extensionDetails[MAX_EXTENSION];
    int extensionDetailsCount[MAX_EXTENSION] = {0};

    sprintf(cmd, "grep -o 'categorized/.*' %s | cut -d '/' -f 2- | sort -u", LOG_FILE);

    FILE* file = popen(cmd, "r");

    while (fgets(line, sizeof(line), file)) {
        line[strcspn(line, "\n")] = 0;
        extensionDetails[numFolder] = strdup(line);
        numFolder++;
    }
    pclose(file);

 
    for (i = 0; i < numFolder; i++) {
        sprintf(cmd, "grep -c '> categorized/%s$' log.txt", extensionDetails[i]);

        file = popen(cmd, "r");
        fgets(line, sizeof(line), file);
        extensionDetailsCount[i] = atoi(line);

        pclose(file);
    }


    for (i = 0; i < numFolder - 1; i++) {
        for (j = i + 1; j < numFolder; j++) {
            if (extensionDetailsCount[i] > extensionDetailsCount[j]) {
                char* temp = extensionDetails[i];
                extensionDetails[i] = extensionDetails[j];
                extensionDetails[j] = temp;

                int tempCount = extensionDetailsCount[i];
                extensionDetailsCount[i] = extensionDetailsCount[j];
                extensionDetailsCount[j] = tempCount;
            }
        }
    }

    printf("Folder Count List:\n");
    for (i = 0; i < numFolder; i++) {
        printf("%s : %d\n", extensionDetails[i], extensionDetailsCount[i]);
    }
}

void listExtension(int extension_file_num[], char** extensions, int num_extension) {
    char cmd[256];
    int count;

    FILE* fp;
    for (int i = 0; i < num_extension; i++) {
        sprintf(cmd, "grep -c \"> categorized/%s\" log.txt", extensions[i]);

        fp = popen(cmd, "r");
        fscanf(fp, "%d", &extension_file_num[i]);

        pclose(fp);
    }

    for (int i = 0; i < num_extension - 1; i++) {
        for (int j = i + 1; j < num_extension; j++) {
            if (extension_file_num[i] > extension_file_num[j]) {
                int temp = extension_file_num[i];
                extension_file_num[i] = extension_file_num[j];
                extension_file_num[j] = temp;

                char temp2[10];
                strcpy(temp2, extensions[i]);
                strcpy(extensions[i], extensions[j]);
                strcpy(extensions[j], temp2);
            }
        }
    }

    printf("Extension List:\n");
    for (int i = 0; i < num_extension; i++) {
        printf("%s : %d\n", extensions[i], extension_file_num[i]);
    }
}


```


## Source Code
## unzip.c
```sh
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <math.h>
#include <pthread.h>

int main() {
    char* url = "https://drive.google.com/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
    char* downloadName = "hehe.zip";
    int status;
    pid_t child;

    child = fork();
    if (child == 0) {
        char* args[] = {"wget", "-O", downloadName, url, NULL};
        execv("/bin/wget", args);
    }
    waitpid(child, &status, 0);

    child = fork();
    if (child == 0) {
        char* args[] = {"unzip", downloadName, NULL};
        execv("/bin/unzip", args);
    }
    waitpid(child, &status, 0);

    child = fork();
    if (child == 0) {
        char* args[] = {"rm", downloadName, NULL};
        execv("/bin/rm", args);
    }
    waitpid(child, &status, 0);
}
```
## categorize.c 
```bash
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <math.h>
#include <pthread.h>

#define MAX_EXTENSION 100

char* EXTENSIONS_FILE = "extensions.txt";
char* MAX_FILE = "max.txt";
char* LOG_FILE = "log.txt";
char* CATEGORY_FOLDER = "categorized";
char* DIRECTORY = "files";
int* extension_file_num;
int other_extension_num;

typedef struct ThreadArgument {
    char* dirname;
    char** extensions;
    int count_extension;
    int max;
} ThreadArgument;

void fileProcessing(char* path, int num_extension, char** extensions, int max);
void directoryProcessing(char* dirname, int num_extension, char** extensions, int max);
void* directory_process_routine(void* arg);
void printExtfilecount(int ext_file_num[], char** extensions, int n_ext);

void createDir(char* dirname);
void move(char* src, char* dest);
char* timeFormat();

void accessLog(char* path);
void moveLog(char* ext, char* src, char* dest);
void madeLog(char* folder);


int main() {
    extension_file_num = calloc(MAX_EXTENSION, sizeof(int));
    other_extension_num = 0;
    char *extensions[MAX_EXTENSION], c;
    int num_extension = 0;
    int pos = 0;
    int ext_max;

    FILE* file = fopen(EXTENSIONS_FILE, "a");
    fprintf(file, "other\n");
    fclose(file);

    file = fopen(EXTENSIONS_FILE, "r");

    char line[MAX_EXTENSION];
    while (fgets(line, MAX_EXTENSION, file)) {
        char* token = strtok(line, " \t\r\n");
        while (token != NULL) {
            token[strcspn(token, "\n")] = 0;
            extensions[num_extension] = strdup(token);
            num_extension++;
            token = strtok(NULL, " \t\r\n");
        }
    }

    file = fopen(MAX_FILE, "r");
    fscanf(file, "%d", &ext_max);
    fclose(file);

    directoryProcessing(DIRECTORY, num_extension, extensions, ext_max);
    printExtfilecount(extension_file_num, extensions, num_extension);

    return 0;
}

void createDir(char* dirname) {
    char command[1000];
    sprintf(command, "mkdir %s", dirname);
    system(command);
}

void move(char* src, char* dest) {
    char command[1000];
    sprintf(command, "mv %s %s", src, dest);
    system(command);
}

char* timeFormat() {
    time_t current_time = time(NULL);
    struct tm* time_info = localtime(&current_time);

    char* formatted_time = malloc(20 * sizeof(char));
    strftime(formatted_time, 20, "%Y-%m-%d %H:%M:%S", time_info);
    return formatted_time;
}

void accessLog(char* path) {
    FILE* logFile = fopen(LOG_FILE, "a");
    fprintf(logFile, "%s ACCESSED %s\n", timeFormat(), path);
    fclose(logFile);
}

void moveLog(char* ext, char* src, char* dest) {
    FILE* logFile = fopen(LOG_FILE, "a");
    fprintf(logFile, "%s MOVED %s file : %s > %s\n", timeFormat(), ext, src, dest);
    fclose(logFile);
}

void madeLog(char* folder) {
    FILE* logFile = fopen(LOG_FILE, "a");
    fprintf(logFile, "%s MADE %s\n", timeFormat(), folder);
    fclose(logFile);
}

void fileProcessing(char* path, int num_extension, char** extensions, int max) {
    if (access("categorized", F_OK) != 0) {
        createDir("categorized");
        madeLog("categorized");
    }

    char* filename = basename(path);
    char* lastDot = strrchr(filename, '.');
    char command[1000];

    if (lastDot) {
        *lastDot = '\0';

        char ext[150], ext2[150];
        strcpy(ext, lastDot + 1);
        strcpy(ext2, ext);
        for (int i = 0; ext2[i]; i++) ext2[i] = tolower(ext[i]);

        int ext_idx = -1;
        for (int i = 0; i < num_extension; i++) {
            if (!strcmp(ext2, extensions[i])) {
                ext_idx = i;
                extension_file_num[ext_idx]++;
                break;
            }
        }

        char newPath[200], oldPath[200];

        if (ext_idx >= 0 && extension_file_num[ext_idx] <= max) {
            sprintf(newPath, "categorized/%s", extensions[ext_idx]);
        } else if (ext_idx >= 0) {  
            sprintf(newPath, "categorized/%s (%d)", extensions[ext_idx], (int)ceil(extension_file_num[ext_idx] / (float)max));
        } else {  
            extension_file_num[num_extension - 1]++;
            sprintf(newPath, "categorized/other");
        }

        if (access(newPath, F_OK) != 0) {
            sprintf(command, "mkdir \"%s\"", newPath);
            system(command);
            madeLog(newPath);
        }

        snprintf(oldPath, sizeof(oldPath), "%s.%s", path, ext);
        sprintf(command, "mv '%s' \"%s\"", oldPath, newPath);
        system(command);
        moveLog(ext, oldPath, newPath);
    } else {  
        extension_file_num[num_extension - 1]++;
        if (access("categorized/other", F_OK) != 0) {
            system("mkdir categorized/other");
            madeLog("categorized/other");
        }

        sprintf(command, "mv '%s' categorized/other", path);
        system(command);
        moveLog("other", path, "categorized/other");
    }
}

void directoryProcessing(char* dirname, int num_extension, char** extensions, int max) {
    pthread_t myThreads[200];
    int n_thread = 0;

    DIR* dir;
    struct dirent* entry;
    dir = opendir(dirname);
    accessLog(dirname);

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }
        char path[2000];
        sprintf(path, "%s/%s", dirname, entry->d_name);

        if (entry->d_type == DT_DIR) {
            struct ThreadArgument* threadArg = malloc(sizeof(struct ThreadArgument));
            threadArg->dirname = strdup(path);
            threadArg->count_extension = num_extension;
            threadArg->extensions = extensions;
            threadArg->max = max;

            pthread_create(&myThreads[n_thread], NULL, directory_process_routine, threadArg);
            n_thread++;
        } else {  
            fileProcessing(path, num_extension, extensions, max);
        }
    }
    closedir(dir);

    for (int i = 0; i < n_thread; i++) {
        pthread_join(myThreads[i], NULL);
    }
}


void* directory_process_routine(void* arg) {
    struct ThreadArgument* threadArg = (struct ThreadArgument*)arg;
    directoryProcessing(threadArg->dirname, threadArg->count_extension, threadArg->extensions, threadArg->max);
    pthread_exit(NULL);
}

void printExtfilecount(int ext_file_num[], char** extensions, int n_ext) {

    for (int i = 0; i < n_ext - 1; i++) {
        for (int j = i + 1; j < n_ext; j++) {
            if (ext_file_num[i] > ext_file_num[j]) {
                int temp = ext_file_num[i];
                ext_file_num[i] = ext_file_num[j];
                ext_file_num[j] = temp;

                char temp2[10];
                strcpy(temp2, extensions[i]);
                strcpy(extensions[i], extensions[j]);
                strcpy(extensions[j], temp2);
            }
        }
    }
    for (int i = 0; i < n_ext; i++) {
        printf("%s : %d\n", extensions[i], ext_file_num[i]);
    }
}
```
## logchecker.c
```bash
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <math.h>
#include <pthread.h>

#define MAX_EXTENSION 100
#define MAX_LINE_LENGTH 100

char* LOG_FILE = "log.txt";
int* extension_file_num;

int extensionRead(char** extensions);
int countAcessed();
void listExtension(int extension_file_num[], char** extensions, int num_extension);
void countListFolder();

int main() {
    extension_file_num = calloc(MAX_EXTENSION, sizeof(int));
    char* extensions[MAX_EXTENSION];
    int num_extension = extensionRead(extensions);

    printf("ACCESSED %d times\n", countAcessed());
    printf("----------------------------------\n");
    countListFolder();
    printf("----------------------------------\n");
    listExtension(extension_file_num, extensions, num_extension);

    return 0;
}

int extensionRead(char** extensions) {
    char cmd[MAX_LINE_LENGTH];
    char line[MAX_LINE_LENGTH];
    int num_extension = 0;

    sprintf(cmd, "grep -oP '(?<=categorized/)[^\\s]*' %s | sort -u", LOG_FILE);

    FILE* fp = popen(cmd, "r");

    if (fp == NULL) {
        printf("Failed to execute command\n");
        return 1;
    }

    while (fgets(line, MAX_LINE_LENGTH, fp) != NULL) {
        line[strcspn(line, "\n")] = 0;
        extensions[num_extension] = malloc(strlen(line) + 1);
        strcpy(extensions[num_extension], line);
        num_extension++;
    }
    pclose(fp);
    return num_extension;
}

int countAcessed() {
    char accessed[] = "ACCESSED";
    char line[100];
    int count = 0;

    FILE* file = fopen(LOG_FILE, "r");
    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, accessed)) {
            count++;
        }
    }
    fclose(file);
    return count;
}

void countListFolder() {
    char cmd[MAX_LINE_LENGTH];
    char line[MAX_LINE_LENGTH];
    int i, j, numFolder = 0;

    char* extensionDetails[MAX_EXTENSION];
    int extensionDetailsCount[MAX_EXTENSION] = {0};

    sprintf(cmd, "grep -o 'categorized/.*' %s | cut -d '/' -f 2- | sort -u", LOG_FILE);

    FILE* file = popen(cmd, "r");

    while (fgets(line, sizeof(line), file)) {
        line[strcspn(line, "\n")] = 0;
        extensionDetails[numFolder] = strdup(line);
        numFolder++;
    }
    pclose(file);

 
    for (i = 0; i < numFolder; i++) {
        sprintf(cmd, "grep -c '> categorized/%s$' log.txt", extensionDetails[i]);

        file = popen(cmd, "r");
        fgets(line, sizeof(line), file);
        extensionDetailsCount[i] = atoi(line);

        pclose(file);
    }


    for (i = 0; i < numFolder - 1; i++) {
        for (j = i + 1; j < numFolder; j++) {
            if (extensionDetailsCount[i] > extensionDetailsCount[j]) {
                char* temp = extensionDetails[i];
                extensionDetails[i] = extensionDetails[j];
                extensionDetails[j] = temp;

                int tempCount = extensionDetailsCount[i];
                extensionDetailsCount[i] = extensionDetailsCount[j];
                extensionDetailsCount[j] = tempCount;
            }
        }
    }

    printf("Folder Count List:\n");
    for (i = 0; i < numFolder; i++) {
        printf("%s : %d\n", extensionDetails[i], extensionDetailsCount[i]);
    }
}

void listExtension(int extension_file_num[], char** extensions, int num_extension) {
    char cmd[256];
    int count;

    FILE* fp;
    for (int i = 0; i < num_extension; i++) {
        sprintf(cmd, "grep -c \"> categorized/%s\" log.txt", extensions[i]);

        fp = popen(cmd, "r");
        fscanf(fp, "%d", &extension_file_num[i]);

        pclose(fp);
    }

    for (int i = 0; i < num_extension - 1; i++) {
        for (int j = i + 1; j < num_extension; j++) {
            if (extension_file_num[i] > extension_file_num[j]) {
                int temp = extension_file_num[i];
                extension_file_num[i] = extension_file_num[j];
                extension_file_num[j] = temp;

                char temp2[10];
                strcpy(temp2, extensions[i]);
                strcpy(extensions[i], extensions[j]);
                strcpy(extensions[j], temp2);
            }
        }
    }

    printf("Extension List:\n");
    for (int i = 0; i < num_extension; i++) {
        printf("%s : %d\n", extensions[i], extension_file_num[i]);
    }
}
```
## Tes Output
## unzip.c
![image](/uploads/b962df4ceaab1290480a4472a0275840/image.png)
## categorize.c 
![image](/uploads/cd4ced74fac5c65a818b616889815e38/image.png)
## logchecker.c 
![image](/uploads/a16b866cebf57e13bd51399c6052eacc/image.png)
