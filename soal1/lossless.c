#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <ctype.h>

#define CHAR_MAX 1024

// deklarasi struct node binary tree huffman
struct huffmanNode {
    int frekuensi;
    char huruf;
    struct huffmanNode *parent; // pointers (parent, anak_kiri and anak_kanan)
    struct huffmanNode *anak_kanan; // pointers (parent, anak_kiri and anak_kanan)
    struct huffmanNode *anak_kiri; // pointers (parent, anak_kiri and anak_kanan)
};

// Membuat node baru untuk huffman binary tree
struct huffmanNode *nodeBaruHuff(char huruf, int frekuensi){
    struct huffmanNode *node = (struct huffmanNode *)malloc(sizeof(struct huffmanNode));
    node->huruf = huruf;
    node->frekuensi = frekuensi;
    node->anak_kiri = NULL;
    node->anak_kanan = NULL;
    return node;
}

// Membandingkan frekuensi dari 2 node huffman
int banding_nodeArray(const void *p, const void *x){
    return (*(struct huffmanNode **)p)->frekuensi - (*(struct huffmanNode **)x)->frekuensi;
}

// Membuat binary tree huffman
struct huffmanNode *buat_huffman_tree(char *data, int *frekuensi){
    //inisialisasi array dari node huffman untuk tiap simbol pada data
    struct huffmanNode *nodeArray[CHAR_MAX] = {NULL};
    int n = 0;
    int i = 0;
    while (i < strlen(data)){
        char huruf = toupper(data[i]);
        if (isalpha(huruf)) {
            // mengecek jika huruf sudah pernah di temukan
            int j = 0;
            while (j < n)
            {
                if (nodeArray[j]->huruf == huruf)
                {
                    nodeArray[j]->frekuensi++;
                    break;
                }
                j++;
            }
            // jika belum, dibuat node baru
            if (j == n)
            {
                nodeArray[j] = nodeBaruHuff(huruf, frekuensi[huruf]);
                n++;
            }
        }
        i++;
    }
    // sorting node huffman berdasarkan frekuensi
    qsort(nodeArray, n, sizeof(struct huffmanNode *), banding_nodeArray);

    // membuat binary tree huffman 
    while (n > 1) {
        struct huffmanNode *nodeP = nodeArray[0];
        struct huffmanNode *nodeX = nodeArray[1];
        struct huffmanNode *parent = nodeBaruHuff('$', nodeP->frekuensi + nodeX->frekuensi);
        parent->anak_kiri = nodeP;
        parent->anak_kanan = nodeX;
        // menghapus nodeP dan nodeX
        for (int i = 2; i < n; i++) {
            memcpy(&nodeArray[i - 2], &nodeArray[i], sizeof(*nodeArray));
        }
        nodeArray[n - 2] = parent;
        n--;
        // sorting lagi
        qsort(nodeArray, n, sizeof(*nodeArray), banding_nodeArray);
    }
    // nodearray merupakan root dari huffman binary tree
    return nodeArray[0];

}

// menghitung jumlah bit yang dibutuhkan untuk mengompres data menggunakan Huffman binary tree
int total_dan_kompresi(struct huffmanNode *root, int length){
    if (!root){
        return 0;
    }
    else if(root->anak_kiri == NULL && root->anak_kanan == NULL){
        return root->frekuensi * length;
    }
    else{
        return total_dan_kompresi(root->anak_kiri, length + 1) + total_dan_kompresi(root->anak_kanan, length + 1);
    }
}

// C. membaca Huffman tree dari file terkompresi
struct huffmanNode *baca_huffman_tree(char *namaFile){
    FILE *file = fopen(namaFile, "r");
    if (file == NULL){
        printf("Gagal membuka file\n");
        exit(1);
    }
    //membaca karakter pertama dr file
    char ch = fgetc(file);
    if (ch == EOF){
        printf("File kosong\n");
        exit(1);
    }
    //root dr huffman tree yg dibaca file
    struct huffmanNode *root = nodeBaruHuff('$', 0);
    struct huffmanNode *cur_node = root;

    while (ch != EOF){ 
        //karakter = 0, dicek ada anak kiri, akan dibuat node baru
        if (ch == '0'){
            if (cur_node->anak_kiri == NULL){
                cur_node->anak_kiri = nodeBaruHuff('$', 0);
            }
            cur_node = cur_node->anak_kiri;
        }
        //karakter = 1, cek anak kanan
        else if (ch == '1'){
            if (cur_node->anak_kanan == NULL){
                cur_node->anak_kanan = nodeBaruHuff('$', 0);
            }
            cur_node = cur_node->anak_kanan;
        }
        ch = fgetc(file);
    }
    fclose(file);
    return root;
}

// mencari kode huffman tiap huruf di binary tree dan dikompress
void cari_huffmanKode(struct huffmanNode *root, char *kode, int length_kode, char *inpFile, char *kompres){
    if (root->anak_kiri == NULL && root->anak_kanan == NULL) {
        kode[length_kode] = '\0';
        printf("%c: %s\n", root->huruf, kode);
        int i = 0;
        while (i < strlen(inpFile)){
            if (toupper(inpFile[i]) == root->huruf){
                strcat(kompres, kode);
            }
            i++;
        }
        return;
    } 
    kode[length_kode] = '0';
    cari_huffmanKode(root->anak_kiri, kode, length_kode + 1, inpFile, kompres);
    kode[length_kode] = '1';
    cari_huffmanKode(root->anak_kanan, kode, length_kode + 1, inpFile, kompres);
}


int main(int argc, char *argv[]){
    //mengecek jumlah argument 
    if (argc != 2){
        fprintf(stderr, "Usage: %s [namaFile]\n", argv[0]);
        exit(1);
    }

    char *namaFile = argv[1];
    int fd = open(namaFile, O_RDWR);
    if (fd == -1){
        perror("tidak bisa mengakses file");
        return 1;
    }

    // inisialisasi 2 pipe
    int fd1[2]; // menyimpan dua ujung pipa pertama
    int fd2[2]; // menyimpan dua ujung pipa kedua

    if (pipe(fd1) < 0 || pipe(fd2) < 0) {
        perror("Pipe Failed");
        return 1;
    }

    pid_t p;
    p = fork();

    if (p < 0){
        fprintf(stderr, "fork Failed");
        return 1;
    }

    // Parent process
    else if (p > 0){
        // Menutup pembacaan akhir pipa pertama
        close(fd1[0]); 
        // Menutup pembacaan akhir pipa kedua
        close(fd2[1]); 

        int frekuensi[CHAR_MAX] = {0};
        char huruf;
        int total_bits = 0;
        while (read(fd, &huruf, 1) > 0){
            if (isalpha(huruf)){
                huruf = toupper(huruf);
                frekuensi[(int)huruf]++;
                // Menambah 8 bit di setiap huruf yang ditemukan
                total_bits += 8;
            }
        }

        int i = 0;
        while (i < CHAR_MAX){
            if (frekuensi[i] > 0){
                if (isalpha(i)) {
                    printf("%c: %d\n", i, frekuensi[i]);
                } else {
                    printf("'%c': %d\n", i, frekuensi[i]);
                }
            }
            i++;
        }

        printf("\nSebelum kompress(bit): %d\n", total_bits);
        close(fd);

        // mengirim frekuensi ke child process
        write(fd1[1], frekuensi, sizeof(frekuensi));
        close(fd1[1]);

        // menunggu untuk mengirim bit yang dikompress
        int bit_kompres;
        read(fd2[0], &bit_kompres, sizeof(bit_kompres));
        close(fd2[0]);

        float perbandinganKompres = (float)bit_kompres / total_bits;

        return 0;
    }
    // Child process
    else{
        close(fd1[1]); // menutup tulisan ujung pipa pertama
        close(fd2[0]); // menutup tulisan ujung pipa kedua

        // membaca frekuensi dari parent process
        int frekuensi[CHAR_MAX] = {0};
        read(fd1[0], frekuensi, sizeof(frekuensi));
        close(fd1[0]);

        //mengoversi isi file ke bentuk frekuensi masing-masing karakter
        char inpFile[CHAR_MAX * 100] = "";
        int i = 0;
        while (i < CHAR_MAX){
            int j = 0;
            while (j < frekuensi[i]){
                char huruf = (char)i;
                strncat(inpFile, &huruf, 1);
                j++;
            }
            i++;
        }

        struct huffmanNode *root = buat_huffman_tree(inpFile, frekuensi);
        char kode[CHAR_MAX] = "";
        char kompres[CHAR_MAX * 100] = "";
        cari_huffmanKode(root, kode, 0, inpFile, kompres);

        int bit_kompres = strlen(kompres);
        printf("Setelah kompress(bit): %d\n", bit_kompres);
        float perbandinganKompres = (float)bit_kompres / total_dan_kompresi(root, 0);
        printf("Perbandingan: %.2f\n", perbandinganKompres);

        // mengirim hasil kompresi ke parent
        write(fd2[1], &bit_kompres, sizeof(bit_kompres));
        close(fd2[1]);
        exit(0);
    }
}

