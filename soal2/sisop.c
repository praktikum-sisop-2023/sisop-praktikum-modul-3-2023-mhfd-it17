#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>

//mendefinisikan array 2D yang akan digunakan untuk menyimpan matriks hasil perkalian yang diambil dari shared memory.
int hasil[4][5];

//mendefinisikan tipe data baru yaitu struct Index yang berisi dua anggota, row dan col. Namun, dalam kasus ini struct tersebut tidak digunakan.
typedef struct {
    int row;
    int col;
} Index;

//fungsi rekursif untuk menghitung faktorial dari suatu angka.
unsigned long long factorial(int n) {
    if (n == 0 || n == 1)
        return 1;
    else
        return n * factorial(n - 1);
}

//fungsi yang akan menghitung faktorial dari elemen matriks pada indeks yang ditentukan dan mencetaknya.
void calculate_factorial_without_thread(int i, int j) {
    printf("%llu ", factorial(hasil[i][j]));
}

int main() {
    //mengambil segmen shared memory dengan kunci 5678 dan memasangkan segmen tersebut ke alamat memori proses.
    key_t key2 = 5678;
    int shmid2 = shmget(key2, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix2)[5] = shmat(shmid2, NULL, 0);
    
    printf("Matriks Hasil Perkalian:\n");
    //mengisi array hasil dengan data dari shared memory dan mencetaknya ke konsol.
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            hasil[i][j] = shared_matrix2[i][j];
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriks Faktorial:\n");
    //mencatat waktu mulai dan melakukan perhitungan faktorial untuk setiap elemen pada matriks tanpa menggunakan thread.
    clock_t start = clock();
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            calculate_factorial_without_thread(i, j);
        }
        printf("\n");
    }

    //Setelah semua perhitungan faktorial selesai, program mencatat waktu selesai dan menghitung durasi yang dibutuhkan untuk melakukan semua perhitungan tersebut. 
    clock_t end = clock();
    double time_taken = (double)(end - start) / CLOCKS_PER_SEC;
    printf("\nWaktu yang dibutuhkan tanpa multithreading: %f detik\n", time_taken);

    //Setelah selesai, program akan melepaskan segmen shared memory dari alamat memori proses dan kemudian menghapus segmen shared memory tersebut.
    shmdt(shared_matrix2);
    shmctl(shmid2, IPC_RMID, NULL);

    return 0;
}
