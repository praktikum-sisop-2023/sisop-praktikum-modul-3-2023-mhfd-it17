#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>

//mendefinisikan matriks dan variabel indeks yang akan digunakan
int main() {
    int matriks1[4][2], matriks2[2][5], hasil[4][5], i, j, k;

    //menginisialisasi generator angka acak dengan waktu sistem saat ini sebagai seed.
    srand(time(NULL));
    
    printf("Matriks Pertama:\n");
    //mengisi matriks pertama dengan angka acak antara 1 dan 5 (inklusif) dan mencetaknya ke konsol.
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 5 + 1;
            printf("%d ", matriks1[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriks Kedua:\n");
    //mengisi matriks kedua dengan angka acak antara 1 dan 4 (inklusif) dan mencetaknya ke konsol.
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 5; j++) {
            matriks2[i][j] = rand() % 4 + 1;
            printf("%d ", matriks2[i][j]);
        }
        printf("\n");
    }

    printf("\nHasil Perkalian Matriks:\n");
    //melakukan perkalian matriks dan mencetak hasilnya ke konsol.
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            hasil[i][j] = 0;
            for (k = 0; k < 2; k++) {
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    //membuat segmen shared memory baru atau mengambil segmen yang sudah ada dengan kunci 1234 dan memasangkan segmen tersebut ke alamat memori proses.
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix)[5] = shmat(shmid, NULL, 0);

    //mengisi shared memory dengan hasil perkalian matriks.
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            shared_matrix[i][j] = hasil[i][j];
        }
    }

    //Setelah selesai mengisi shared memory, proses melepaskan (detaches) shared memory tersebut dengan fungsi shmdt()
    shmdt(shared_matrix);

    //membuat atau mengakses segmen shared memory lain dengan kunci 5678 dan menghubungkannya dengan alamat memori proses.
    key_t key2 = 5678;
    int shmid2 = shmget(key2, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix2)[5] = shmat(shmid2, NULL, 0);

    //mengisi shared memory kedua dengan hasil perkalian matriks.
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            shared_matrix2[i][j] = hasil[i][j];
        }
    }

    //setelah selesai mengisi shared memory, proses melepaskan shared memory tersebut.
    shmdt(shared_matrix2);

    return 0;
}