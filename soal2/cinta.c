#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>

//mendefinisikan array 2D yang akan digunakan untuk menyimpan matriks hasil perkalian yang diambil dari shared memory.
int hasil[4][5];

//mendefinisikan tipe data baru yaitu struct Index yang berisi dua anggota, row dan col.
typedef struct {
    int row;
    int col;
} Index;

//fungsi rekursif untuk menghitung faktorial dari suatu angka.
unsigned long long factorial(int n) {
    if (n == 0 || n == 1)
        return 1;
    else
        return n * factorial(n - 1);
}

//fungsi yang akan dijalankan oleh setiap thread. Fungsi ini mengambil argumen berupa void * yang nantinya akan di-cast ke Index *. Fungsi ini mencetak faktorial dari elemen matriks pada indeks yang ditentukan.
void *calculate_factorial(void *idx) {
    Index *index = (Index *)idx;
    printf("%llu ", factorial(hasil[index->row][index->col]));
}

int main() {

    //mengambil segmen shared memory dengan kunci 1234 dan memasangkan segmen tersebut ke alamat memori proses.
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    int (*shared_matrix)[5] = shmat(shmid, NULL, 0);
    
    printf("Matriks Hasil Perkalian:\n");
    //mengisi array hasil dengan data dari shared memory dan mencetaknya ke konsol.
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            hasil[i][j] = shared_matrix[i][j];
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriks Faktorial:\n");
    //mencatat waktu mulai dan membuat thread untuk setiap elemen
    clock_t start = clock();
    pthread_t threads[4][5];
    //menciptakan sebuah thread untuk setiap elemen pada matriks. Setiap thread akan menjalankan fungsi calculate_factorial dengan argument berupa pointer ke struktur Index yang berisi indeks baris dan kolom dari elemen yang akan dihitung faktorialnya.
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            Index *index = malloc(sizeof(Index));
            index->row = i;
            index->col = j;
            pthread_create(&threads[i][j], NULL, calculate_factorial, (void *)index);
        }
    }

    //Setelah semua thread dibuat, program akan menunggu setiap thread selesai menjalankan tugasnya dengan menggunakan fungsi pthread_join(). Setelah semua thread pada baris matriks selesai, program akan mencetak baris baru.
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            pthread_join(threads[i][j], NULL);
        }
        printf("\n");
    }

    //Setelah semua perhitungan faktorial selesai, program mencatat waktu selesai dan menghitung durasi yang dibutuhkan untuk melakukan semua perhitungan tersebut. Durasi ini kemudian dicetak ke konsol.
    clock_t end = clock();
    double time_taken = (double)(end - start) / CLOCKS_PER_SEC;
    printf("\nWaktu yang dibutuhkan dengan multithreading: %f detik\n", time_taken);

    //Setelah selesai, program akan melepaskan segmen shared memory dari alamat memori proses dan kemudian menghapus segmen shared memory tersebut.
    shmdt(shared_matrix);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
