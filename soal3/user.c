#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mqueue.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define QUEUE_NAME "/message_queue"
#define PERMISSIONS 0666
#define MAX_MSG_SIZE 256

int main() {

    //Mendeklarasikan variabel-variabel yang diperlukan: mq untuk menyimpan descriptor dari message queue, attr untuk menyimpan atribut-atribut dari message queue, dan buffer untuk menyimpan pesan yang akan dikirim ke message queue.
    mqd_t mq;
    struct mq_attr attr;
    char buffer[MAX_MSG_SIZE];

    //Menginisialisasi struktur attr dengan nilai-nilai default.
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MAX_MSG_SIZE;
    attr.mq_curmsgs = 0;

    //membuka message queue dengan nama yang didefinisikan sebelumnya dan mode "write only", lalu menyimpan descriptor-nya ke dalam variabel mq.
    mq = mq_open(QUEUE_NAME, O_WRONLY);
    if (mq == -1) {
        perror("mq_open");
        exit(1);
    }

    while (1) {
        printf("Enter command (DECRYPT, LIST, PLAY, or QUIT): ");

        //Membaca perintah dari user dan menyimpannya ke dalam buffer.
        fgets(buffer, MAX_MSG_SIZE, stdin);

        //Menghilangkan karakter newline dari akhir buffer.
        buffer[strcspn(buffer, "\n")] = '\0';

        if (strcmp(buffer, "QUIT") == 0) {
            break;
        } else if (strncmp(buffer, "PLAY ", 5) == 0) {
            const char *song_title = buffer + 5;
            printf("USER %d PLAYING \"%s\"\n", getpid(), song_title);
        }

        if (mq_send(mq, buffer, strlen(buffer), 0) == -1) {
            perror("mq_send");
            exit(1);
        }
    }

    mq_close(mq);

    return 0;
}
