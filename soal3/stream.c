#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mqueue.h>
#include <sys/stat.h>
#include <json-c/json.h>
#include <ctype.h>
#include <stdbool.h>

// Mendefinisikan nama antrian pesan.
#define QUEUE_NAME "/message_queue"

//Mendefinisikan izin file dengan mode baca/tulis untuk pengguna, grup, dan orang lain.
#define PERMISSIONS 0666
#define MAX_MESSAGES 10
#define MAX_MSG_SIZE 256

//Mendefinisikan ukuran buffer untuk menerima pesan.
#define BUFFER_SIZE (MAX_MSG_SIZE + 10)

//membandingkan dua string dengan tidak memperhatikan case (huruf besar atau kecil). Fungsi ini digunakan oleh qsort() untuk mengurutkan playlist.
int compare_str(const void *a, const void *b) {
    const char *str_a = *(const char **)a;
    const char *str_b = *(const char **)b;
    return strcasecmp(str_a, str_b);
}

//mengurutkan playlist dan menulisnya ke file output.
void sort_and_write_playlist(const char **playlist, size_t song_count, const char *output_file) {
    qsort(playlist, song_count, sizeof(char *), compare_str);

    FILE *out = fopen(output_file, "w");
    if (out == NULL) {
        perror("fopen");
        exit(1);
    }

    for (size_t i = 0; i < song_count; i++) {
        fprintf(out, "%s\n", playlist[i]);
    }

    fclose(out);
}

// Fungsi-fungsi decrypt
// mendekripsi string dengan algoritma ROT13.
void rot13(char *str) {
    for (int i = 0; str[i]; i++) {
        char c = str[i];
        if (c >= 'a' && c <= 'z') {
            str[i] = ((c - 'a' + 13) % 26) + 'a';
        } else if (c >= 'A' && c <= 'Z') {
            str[i] = ((c - 'A' + 13) % 26) + 'A';
        }
    }
}

// mendekripsi string yang dienkripsi dengan Base64.
void base64_decode(const char *src, char *dst) {
    char command[512];
    snprintf(command, sizeof(command), "echo '%s' | base64 -d > decoded.txt", src);
    system(command);

    FILE *file = fopen("decoded.txt", "r");
    if (file == NULL) {
        perror("fopen");
        exit(1);
    }
    fgets(dst, 256, file);
    fclose(file);
    remove("decoded.txt");
}

// mendekripsi string yang dienkripsi dalam bentuk hexadecimal.
void hex_decode(const char *src, char *dst) {
    size_t len = strlen(src);
    for (size_t i = 0; i < len; i += 2) {
        unsigned int byte;
        sscanf(src + i, "%2x", &byte);
        dst[i / 2] = byte;
    }
    dst[len / 2] = '\0';
}

// membaca file JSON yang berisi daftar lagu yang dienkripsi, mendekripsinya, dan menulis hasilnya ke file output.
void decrypt_json(const char *input_file, const char *output_file) {
    json_object *root, *song_entry;
    enum json_tokener_error error = json_tokener_success;

    FILE *fp = fopen(input_file, "r");
    if (fp == NULL) {
        perror("fopen");
        exit(1);
    }

    fseek(fp, 0, SEEK_END);
    size_t file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char *file_content = malloc(file_size + 1);
    if (file_content == NULL) {
        perror("malloc");
        exit(1);
    }
    fread(file_content, 1, file_size, fp);
    fclose(fp);

    root = json_tokener_parse_verbose(file_content, &error);
    if (error != json_tokener_success) {
        fprintf(stderr, "Error parsing JSON: %s\n", json_tokener_error_desc(error));
        exit(1);
    }

    FILE *out = fopen(output_file, "w");
    if (out == NULL) {
        perror("fopen");
        exit(1);
    }

    size_t song_count = json_object_array_length(root);
    const char **decrypted_songs = malloc(song_count * sizeof(char *));
    if (decrypted_songs == NULL) {
        perror("malloc");
        exit(1);
    }

    for (size_t i = 0; i < song_count; i++) {
        song_entry = json_object_array_get_idx(root, i);
        const char *method = json_object_get_string(json_object_object_get(song_entry, "method"));
        const char *song = json_object_get_string(json_object_object_get(song_entry, "song"));

        char *decrypted_song = malloc(MAX_MSG_SIZE);
        if (decrypted_song == NULL) {
            perror("malloc");
            exit(1);
        }
        strcpy(decrypted_song, song);

        if (strcmp(method, "rot13") == 0) {
            rot13(decrypted_song);
        } else if (strcmp(method, "base64") == 0) {
            base64_decode(song, decrypted_song);
        } else if (strcmp(method, "hex") == 0) {
            hex_decode(song, decrypted_song);
        } else {
            fprintf(stderr, "Unknown decryption method: %s\n", method);
            exit(1);
        }

        decrypted_songs[i] = decrypted_song;
    }

    sort_and_write_playlist(decrypted_songs, song_count, output_file);

    for (size_t i = 0; i < song_count; i++) {
        free((void *)decrypted_songs[i]);
    }
    free(decrypted_songs);
    json_object_put(root);
    free(file_content);
}

// membaca file output dan mencetak setiap lagu. Ini digunakan untuk menampilkan daftar lagu.
void list_songs(const char *output_file) {
    FILE *fp = fopen(output_file, "r");
    if (fp == NULL) {
        perror("fopen");
        exit(1);
    }

    char line[256];
    while (fgets(line, sizeof(line), fp)) {
        printf("%s", line);
    }

    fclose(fp);
}

// mencari lagu yang diberikan dalam file playlist dan mencetaknya. Jika lagu tidak ditemukan, fungsi ini mencetak pesan bahwa lagu tersebut tidak ada.
void play_song(const char *song_title, const char *playlist_file) {
    FILE *fp = fopen(playlist_file, "r");
    if (fp == NULL) {
        perror("fopen");
        exit(1);
    }

    char line[256];
    int song_count = 0;
    bool found = false;
    while (fgets(line, sizeof(line), fp)) {
        line[strcspn(line, "\n")] = '\0';
        if (strcasestr(line, song_title) != NULL) {
            found = true;
            song_count++;
            printf("%d. %s\n", song_count, line);
        }
    }

    fclose(fp);

    if (!found) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song_title);
    } else {
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", song_count, song_title);
    }
}

int main() {
    mqd_t mq;
    struct mq_attr attr;

    //mendefinisikan buffer untuk pesan dan mengatur atribut-antribut untuk message queue yang akan dibuat
    char buffer[BUFFER_SIZE];

    attr.mq_flags = 0;
    attr.mq_maxmsg = MAX_MESSAGES;
    attr.mq_msgsize = MAX_MSG_SIZE;
    attr.mq_curmsgs = 0;

    //membuka atau membuat (jika belum ada) sebuah message queue dengan nama QUEUE_NAME, mode hanya-baca, dan permissions yang telah ditentukan.
    mq = mq_open(QUEUE_NAME, O_CREAT | O_RDONLY, PERMISSIONS, &attr);
    if (mq == -1) {
        perror("mq_open");
        exit(1);
    }

    //membaca pesan dari message queue dalam loop tak-terbatas. 
    while (1) {
        ssize_t bytes_read;
        bytes_read = mq_receive(mq, buffer, BUFFER_SIZE, NULL);
        if (bytes_read <= 0) {
            perror("mq_receive");
            exit(1);
        }
        buffer[bytes_read] = '\0';

        //mengecek isi pesan dan melakukan aksi sesuai pesan tersebut
        if (strcmp(buffer, "DECRYPT") == 0) {
            decrypt_json("song-playlist.json", "playlist.txt");
        } else if (strcmp(buffer, "LIST") == 0) {
            list_songs("playlist.txt");
        } else if (strncmp(buffer, "PLAY ", 5) == 0) {
            const char *song_title = buffer + 5;
            play_song(song_title, "playlist.txt");
        } else {
            printf("Invalid command: %s\n", buffer);
        }
    }

    mq_close(mq);
    mq_unlink(QUEUE_NAME);

    return 0;
}
