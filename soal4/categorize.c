#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <math.h>
#include <pthread.h>

#define MAX_EXTENSION 100

char* EXTENSIONS_FILE = "extensions.txt";
char* MAX_FILE = "max.txt";
char* LOG_FILE = "log.txt";
char* CATEGORY_FOLDER = "categorized";
char* DIRECTORY = "files";
int* extension_file_num;
int other_extension_num;

typedef struct ThreadArgument {
    char* dirname;
    char** extensions;
    int count_extension;
    int max;
} ThreadArgument;

void fileProcessing(char* path, int num_extension, char** extensions, int max);
void directoryProcessing(char* dirname, int num_extension, char** extensions, int max);
void* directory_process_routine(void* arg);
void printExtfilecount(int ext_file_num[], char** extensions, int n_ext);

void createDir(char* dirname);
void move(char* src, char* dest);
char* timeFormat();

void accessLog(char* path);
void moveLog(char* ext, char* src, char* dest);
void madeLog(char* folder);


int main() {
    extension_file_num = calloc(MAX_EXTENSION, sizeof(int));
    other_extension_num = 0;
    char *extensions[MAX_EXTENSION], c;
    int num_extension = 0;
    int pos = 0;
    int ext_max;

    FILE* file = fopen(EXTENSIONS_FILE, "a");
    fprintf(file, "other\n");
    fclose(file);

    file = fopen(EXTENSIONS_FILE, "r");

    char line[MAX_EXTENSION];
    while (fgets(line, MAX_EXTENSION, file)) {
        char* token = strtok(line, " \t\r\n");
        while (token != NULL) {
            token[strcspn(token, "\n")] = 0;
            extensions[num_extension] = strdup(token);
            num_extension++;
            token = strtok(NULL, " \t\r\n");
        }
    }

    file = fopen(MAX_FILE, "r");
    fscanf(file, "%d", &ext_max);
    fclose(file);

    directoryProcessing(DIRECTORY, num_extension, extensions, ext_max);
    printExtfilecount(extension_file_num, extensions, num_extension);

    return 0;
}

void createDir(char* dirname) {
    char command[1000];
    sprintf(command, "mkdir %s", dirname);
    system(command);
}

void move(char* src, char* dest) {
    char command[1000];
    sprintf(command, "mv %s %s", src, dest);
    system(command);
}

char* timeFormat() {
    time_t current_time = time(NULL);
    struct tm* time_info = localtime(&current_time);

    char* formatted_time = malloc(20 * sizeof(char));
    strftime(formatted_time, 20, "%Y-%m-%d %H:%M:%S", time_info);
    return formatted_time;
}

void accessLog(char* path) {
    FILE* logFile = fopen(LOG_FILE, "a");
    fprintf(logFile, "%s ACCESSED %s\n", timeFormat(), path);
    fclose(logFile);
}

void moveLog(char* ext, char* src, char* dest) {
    FILE* logFile = fopen(LOG_FILE, "a");
    fprintf(logFile, "%s MOVED %s file : %s > %s\n", timeFormat(), ext, src, dest);
    fclose(logFile);
}

void madeLog(char* folder) {
    FILE* logFile = fopen(LOG_FILE, "a");
    fprintf(logFile, "%s MADE %s\n", timeFormat(), folder);
    fclose(logFile);
}

void fileProcessing(char* path, int num_extension, char** extensions, int max) {
    if (access("categorized", F_OK) != 0) {
        createDir("categorized");
        madeLog("categorized");
    }

    char* filename = basename(path);
    char* lastDot = strrchr(filename, '.');
    char command[1000];

    if (lastDot) {
        *lastDot = '\0';

        char ext[150], ext2[150];
        strcpy(ext, lastDot + 1);
        strcpy(ext2, ext);
        for (int i = 0; ext2[i]; i++) ext2[i] = tolower(ext[i]);

        int ext_idx = -1;
        for (int i = 0; i < num_extension; i++) {
            if (!strcmp(ext2, extensions[i])) {
                ext_idx = i;
                extension_file_num[ext_idx]++;
                break;
            }
        }

        char newPath[200], oldPath[200];

        if (ext_idx >= 0 && extension_file_num[ext_idx] <= max) {
            sprintf(newPath, "categorized/%s", extensions[ext_idx]);
        } else if (ext_idx >= 0) {  
            sprintf(newPath, "categorized/%s (%d)", extensions[ext_idx], (int)ceil(extension_file_num[ext_idx] / (float)max));
        } else {  
            extension_file_num[num_extension - 1]++;
            sprintf(newPath, "categorized/other");
        }

        if (access(newPath, F_OK) != 0) {
            sprintf(command, "mkdir \"%s\"", newPath);
            system(command);
            madeLog(newPath);
        }

        snprintf(oldPath, sizeof(oldPath), "%s.%s", path, ext);
        sprintf(command, "mv '%s' \"%s\"", oldPath, newPath);
        system(command);
        moveLog(ext, oldPath, newPath);
    } else {  
        extension_file_num[num_extension - 1]++;
        if (access("categorized/other", F_OK) != 0) {
            system("mkdir categorized/other");
            madeLog("categorized/other");
        }

        sprintf(command, "mv '%s' categorized/other", path);
        system(command);
        moveLog("other", path, "categorized/other");
    }
}

void directoryProcessing(char* dirname, int num_extension, char** extensions, int max) {
    pthread_t myThreads[200];
    int n_thread = 0;

    DIR* dir;
    struct dirent* entry;
    dir = opendir(dirname);
    accessLog(dirname);

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }
        char path[2000];
        sprintf(path, "%s/%s", dirname, entry->d_name);

        if (entry->d_type == DT_DIR) {
            struct ThreadArgument* threadArg = malloc(sizeof(struct ThreadArgument));
            threadArg->dirname = strdup(path);
            threadArg->count_extension = num_extension;
            threadArg->extensions = extensions;
            threadArg->max = max;

            pthread_create(&myThreads[n_thread], NULL, directory_process_routine, threadArg);
            n_thread++;
        } else {  
            fileProcessing(path, num_extension, extensions, max);
        }
    }
    closedir(dir);

    for (int i = 0; i < n_thread; i++) {
        pthread_join(myThreads[i], NULL);
    }
}


void* directory_process_routine(void* arg) {
    struct ThreadArgument* threadArg = (struct ThreadArgument*)arg;
    directoryProcessing(threadArg->dirname, threadArg->count_extension, threadArg->extensions, threadArg->max);
    pthread_exit(NULL);
}

void printExtfilecount(int ext_file_num[], char** extensions, int n_ext) {

    for (int i = 0; i < n_ext - 1; i++) {
        for (int j = i + 1; j < n_ext; j++) {
            if (ext_file_num[i] > ext_file_num[j]) {
                int temp = ext_file_num[i];
                ext_file_num[i] = ext_file_num[j];
                ext_file_num[j] = temp;

                char temp2[10];
                strcpy(temp2, extensions[i]);
                strcpy(extensions[i], extensions[j]);
                strcpy(extensions[j], temp2);
            }
        }
    }
    for (int i = 0; i < n_ext; i++) {
        printf("%s : %d\n", extensions[i], ext_file_num[i]);
    }
}
