#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <math.h>
#include <pthread.h>

int main() {
    char* url = "https://drive.google.com/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
    char* downloadName = "hehe.zip";
    int status;
    pid_t child;

    child = fork();
    if (child == 0) {
        char* args[] = {"wget", "-O", downloadName, url, NULL};
        execv("/bin/wget", args);
    }
    waitpid(child, &status, 0);

    child = fork();
    if (child == 0) {
        char* args[] = {"unzip", downloadName, NULL};
        execv("/bin/unzip", args);
    }
    waitpid(child, &status, 0);

    child = fork();
    if (child == 0) {
        char* args[] = {"rm", downloadName, NULL};
        execv("/bin/rm", args);
    }
    waitpid(child, &status, 0);
}
