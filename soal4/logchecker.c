#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <math.h>
#include <pthread.h>

#define MAX_EXTENSION 100
#define MAX_LINE_LENGTH 100

char* LOG_FILE = "log.txt";
int* extension_file_num;

int extensionRead(char** extensions);
int countAcessed();
void listExtension(int extension_file_num[], char** extensions, int num_extension);
void countListFolder();

int main() {
    extension_file_num = calloc(MAX_EXTENSION, sizeof(int));
    char* extensions[MAX_EXTENSION];
    int num_extension = extensionRead(extensions);

    printf("ACCESSED %d times\n", countAcessed());
    printf("----------------------------------\n");
    countListFolder();
    printf("----------------------------------\n");
    listExtension(extension_file_num, extensions, num_extension);

    return 0;
}

int extensionRead(char** extensions) {
    char cmd[MAX_LINE_LENGTH];
    char line[MAX_LINE_LENGTH];
    int num_extension = 0;

    sprintf(cmd, "grep -oP '(?<=categorized/)[^\\s]*' %s | sort -u", LOG_FILE);

    FILE* fp = popen(cmd, "r");

    if (fp == NULL) {
        printf("Failed to execute command\n");
        return 1;
    }

    while (fgets(line, MAX_LINE_LENGTH, fp) != NULL) {
        line[strcspn(line, "\n")] = 0;
        extensions[num_extension] = malloc(strlen(line) + 1);
        strcpy(extensions[num_extension], line);
        num_extension++;
    }
    pclose(fp);
    return num_extension;
}

int countAcessed() {
    char accessed[] = "ACCESSED";
    char line[100];
    int count = 0;

    FILE* file = fopen(LOG_FILE, "r");
    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, accessed)) {
            count++;
        }
    }
    fclose(file);
    return count;
}

void countListFolder() {
    char cmd[MAX_LINE_LENGTH];
    char line[MAX_LINE_LENGTH];
    int i, j, numFolder = 0;

    char* extensionDetails[MAX_EXTENSION];
    int extensionDetailsCount[MAX_EXTENSION] = {0};

    sprintf(cmd, "grep -o 'categorized/.*' %s | cut -d '/' -f 2- | sort -u", LOG_FILE);

    FILE* file = popen(cmd, "r");

    while (fgets(line, sizeof(line), file)) {
        line[strcspn(line, "\n")] = 0;
        extensionDetails[numFolder] = strdup(line);
        numFolder++;
    }
    pclose(file);

 
    for (i = 0; i < numFolder; i++) {
        sprintf(cmd, "grep -c '> categorized/%s$' log.txt", extensionDetails[i]);

        file = popen(cmd, "r");
        fgets(line, sizeof(line), file);
        extensionDetailsCount[i] = atoi(line);

        pclose(file);
    }


    for (i = 0; i < numFolder - 1; i++) {
        for (j = i + 1; j < numFolder; j++) {
            if (extensionDetailsCount[i] > extensionDetailsCount[j]) {
                char* temp = extensionDetails[i];
                extensionDetails[i] = extensionDetails[j];
                extensionDetails[j] = temp;

                int tempCount = extensionDetailsCount[i];
                extensionDetailsCount[i] = extensionDetailsCount[j];
                extensionDetailsCount[j] = tempCount;
            }
        }
    }

    printf("Folder Count List:\n");
    for (i = 0; i < numFolder; i++) {
        printf("%s : %d\n", extensionDetails[i], extensionDetailsCount[i]);
    }
}

void listExtension(int extension_file_num[], char** extensions, int num_extension) {
    char cmd[256];
    int count;

    FILE* fp;
    for (int i = 0; i < num_extension; i++) {
        sprintf(cmd, "grep -c \"> categorized/%s\" log.txt", extensions[i]);

        fp = popen(cmd, "r");
        fscanf(fp, "%d", &extension_file_num[i]);

        pclose(fp);
    }

    for (int i = 0; i < num_extension - 1; i++) {
        for (int j = i + 1; j < num_extension; j++) {
            if (extension_file_num[i] > extension_file_num[j]) {
                int temp = extension_file_num[i];
                extension_file_num[i] = extension_file_num[j];
                extension_file_num[j] = temp;

                char temp2[10];
                strcpy(temp2, extensions[i]);
                strcpy(extensions[i], extensions[j]);
                strcpy(extensions[j], temp2);
            }
        }
    }

    printf("Extension List:\n");
    for (int i = 0; i < num_extension; i++) {
        printf("%s : %d\n", extensions[i], extension_file_num[i]);
    }
}
